#!/usr/bin/env python3

import argparse
import sys
import os
import shutil

# local import
from src import peptide_table as pt
from src import sequences as seq
from src import taxonomy as ta 
from src import markers
from src import limit
from src import fasta_parsing as fa
#from src import message


"""
Mettre à jour la taxonomie par défaut pour PAMPA web :

python3 -t taxonomie_complete.tsv -p table_de_peptides_1.tsv table_de_peptides_2.tsv (table_d...) -o nom_de_nouvelle_taxonomie.tsv -r
"""

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-r", dest="reroot", help="Set the root to the Lowest Common Ancestor of all the leaves", action='store_true')
    parser.add_argument("-p", dest="peptide_table",nargs='+', help="Peptide table (TSV file).", type=str)
    parser.add_argument("-o", dest="output", help="Output path (should include the output file name)", type=str)
    parser.add_argument("-f", dest="fasta", help="FASTA file that contains new sequences.", type=str)
    parser.add_argument("-d", dest="directory", help="Directory that contains FASTA files.", type=str)
    parser.add_argument("-l", dest="limit",  help="Limit file that contains a list of clades (OX=)", type=str)
    parser.add_argument("-t", dest="taxonomy", help="Taxonomy (TSV file)", type=str, required=True)
    args = parser.parse_args()


    # parsing taxonomy    
    primary_taxonomy=ta.parse_taxonomy_simple_file(args.taxonomy)

    if not (args.peptide_table or args.fasta or args.directory or args.limit):
        # print("Missing input")
        sys.exit()
    
    # parsing models for peptide tables, sequences or limits
    set_of_taxid=set()
    if args.peptide_table :
        set_of_markers = pt.parse_peptide_tables(args.peptide_table, args.limit, primary_taxonomy)
        set_of_taxid.update({m.taxid for m in set_of_markers})
    if args.fasta or args.directory:
        set_of_sequences = fa.build_set_of_sequences(args.fasta, args.directory, None, primary_taxonomy)
        set_of_taxid.update({s.taxid for s in set_of_sequences})
    if args.limit:
        list_of_constraints=limit.parse_limits(args.limit)
        set_of_taxid.update({t for dict in list_of_constraints for t in dict["OX"]}) 


    secondary_taxonomy, lost_taxid=primary_taxonomy.intersection(set_of_taxid)

    
    if len(lost_taxid)>0:
        pass
        # print("The following taxids were not found in "+args.taxonomy)
        # print(lost_taxid)
    ta.table_print(secondary_taxonomy)
        
    ta.create_taxonomy_file(secondary_taxonomy, args.output)

    if args.reroot:
        lca = secondary_taxonomy.lca(set_of_taxid)
        set_of_taxid = secondary_taxonomy.descendants[lca]

        shutil.copy(args.output, args.output+"bis")
        with open(args.output+"bis", "r") as fileIn:
            with open(args.output, "w") as fileOut:
                fileOut.write(fileIn.readline())
                line = fileIn.readline()
                while line != "":
                    taxid = line.split('\t')[0]
                    if taxid in set_of_taxid:
                        if taxid == lca:
                            line_split = line.split('\t')
                            line_split[3] = "-1"
                            fileOut.write('\t'.join(line_split))
                        else:
                            fileOut.write(line)
                    line = fileIn.readline()
        os.remove(args.output+"bis")


    
if __name__ == "__main__":
    main()


    
    
            
