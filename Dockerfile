FROM ubuntu:20.04

ARG DEBIAN_FRONTEND=noninteractive

ARG SERVER_NAME=localhost

ARG HTML_ROOT=/var/www/html

RUN apt-get update && apt-get install -y \
	nginx \
    php-fpm \
    fcgiwrap \
    spawn-fcgi \
    dialog \
    apt-utils \
    python3-pip \
    postgresql \
    git \
	curl \
	wget \
	nano \
    unzip

# Créer lien sympolique python
RUN ln -s /usr/bin/python3 /usr/bin/python

# Python required modules
RUN pip install numpy
RUN pip install biopython
RUN pip install pyteomics
RUN pip install lxml
RUN pip install psycopg2-binary
RUN pip install scipy
RUN pip install pandas
RUN pip install bokeh
RUN pip install beautifulsoup4

# Chrome driver, selenium, etc.
RUN pip install imgkit
RUN pip install html2image
RUN pip install selenium
RUN pip install Pillow

RUN wget -P /root/ https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
RUN apt -y install /root/google-chrome-stable_current_amd64.deb

RUN wget -P /root/ https://storage.googleapis.com/chrome-for-testing-public/123.0.6312.122/linux64/chrome-linux64.zip
RUN unzip -d /root/ /root/chrome-linux64.zip
RUN rm /usr/bin/google-chrome
RUN ln -s /root/chrome-linux64/chrome /usr/bin/google-chrome

RUN wget -P /root/ https://storage.googleapis.com/chrome-for-testing-public/123.0.6312.122/linux64/chromedriver-linux64.zip
RUN unzip -d /root/ /root/chromedriver-linux64.zip
RUN ln -s /root/chromedriver-linux64/chromedriver /usr/bin/
RUN chmod -R 755 /root
RUN chmod -R 766 /root/.cache

# Configuration nginx, FastCGI et serveurs virtuels
COPY ./nginx/fastcgi.conf /etc/nginx/
COPY ./nginx/fastcgi_params /etc/nginx/
COPY ./nginx/fcgiwrap.conf /etc/nginx/
COPY ./nginx/nginx.conf /etc/nginx/
COPY ./nginx/sites-available/pampa /etc/nginx/sites-available/pampa
COPY ./nginx/spawn-fcgi /

RUN sed -i "s/localhost;/localhost $SERVER_NAME;/g" /etc/nginx/sites-available/pampa

RUN ln -s /etc/nginx/sites-available/pampa /etc/nginx/sites-enabled/pampa

# Configuration postgresql
COPY ./postgresql/pg_hba.conf /etc/postgresql/12/main/
RUN chown postgres /etc/postgresql/12/main/pg_hba.conf


# Copie des répertoires html et cgi-bin
COPY ./www/html/ /var/www/html/
RUN mkdir /var/www/cgi-bin
COPY ./www/cgi-bin/ /var/www/cgi-bin/
RUN chown -R www-data:www-data /var/www

# on récupère le logiciel pampa depuis GitHub
# désactivation temporaire pour ne pas mettre à jour la version de pampa
# RUN git clone https://github.com/touzet/pampa.git /var/www/cgi-bin/pampa/pampa/
# ajustement pour version web : pas de print sur sdtout mais envoie d'une réponse CGI de type content-type/html
COPY ./pampa /var/www/cgi-bin/pampa/pampa
COPY ./main_taxonomy_filtering.py /var/www/cgi-bin/pampa/pampa/

# Ajustage des droits
RUN chmod +x /var/www/cgi-bin/pampa/*
RUN mkdir /var/www/cgi-bin/pampa/tmp
RUN mkdir /var/www/html/pampa/result
RUN chmod -R 755 /var/www/cgi-bin/pampa/tmp
RUN chown -R www-data:www-data /var/www/cgi-bin/pampa
RUN chown -R www-data:www-data /var/www/html/pampa

# Copie de la bdd
COPY ./postgresql /var/www/cgi-bin/pampa/postgresql
RUN chmod +x /var/www/cgi-bin/pampa/postgresql/*
RUN chown postgres:postgres /var/www/cgi-bin/pampa/postgresql

# Expose le port 80
EXPOSE 80

# Lancer les différents services au démmarrage (nginx, php-fpm, fastcgiwrap)
CMD service php7.4-fpm start && /spawn-fcgi && /etc/init.d/postgresql start && /var/www/cgi-bin/pampa/postgresql/create_pampa_db && /usr/bin/python /var/www/cgi-bin/pampa/postgresql/fill_taxo.py /var/www/html/pampa/data_pampa/taxonomy_reduced.tsv reduced && /usr/bin/python /var/www/cgi-bin/pampa/postgresql/json_taxo.py /var/www/html/pampa/data_pampa reduced && chown www-data:www-data -R /var/run/fcgiwrap.socket &&  chmod 777 /var/run/fcgiwrap.socket &&  nginx -g "daemon off;"

