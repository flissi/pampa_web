$(document).on("click", "#reference_source", function() {
    if ($("#reference_source input").is(":checked")) {
        $("#block_reference_source_default").hide();
        $("#block_reference_source_user").show();
    } else {
        $("#block_reference_source_default").show();
        $("#block_reference_source_user").hide();
    }
    console.log($("#reference_source input").is(":checked"));
});

$(document).on("click", "#show_warnings", function() {
    $("#warnings_hidden").hide();
    $("#warnings_shown").show();
})

$(document).on("click", "#hide_warnings", function() {
    $("#warnings_shown").hide();
    $("#warnings_hidden").show();
})

$(document).on("click", ".link_img", function() {
    var hash=window.location.href;
    var curitem=hash.split("/");
    var formData = new FormData();
    formData.append("run_id", curitem[5]);
    formData.append("spectrum_name", $(this).data("spectrumname"));
    formData.append("job_name", $(this).data("jobname"));
    formData.append("taxo", $(this).data("taxo"));

    $.ajax({
        url: "/cgi-bin/pampa/render_details.py",
        type: "POST",
        data: formData, 
        dataType: "json",
        contentType: false,
        cache: false,
        processData:false,
        success: function(response){
            setTimeout(function() {
                url_result = response.url_result;
                window.open(url_result, "_blank");
            }, 200);
        },
        error: function(request, status, error){
            $("#main").html(request.responseText);
        }
    });
});


$(document).on("click", "#search", function(){
    var formData = new FormData(document.querySelector("#retrieve"));
    $.ajax({
        url: "/cgi-bin/pampa/retrieve_results.py",
        type: "POST",
        data: formData, 
        dataType: "json",
        contentType: false,
        cache: false,
        processData:false,
        success: function(response){
            setTimeout(function() {
                url_result = response.url_result;
                $('#main').load(url_result+" #center");
                push_in_history(url_result);
            }, 200);
        },
        error: function(request, status, error){
            $("#main").html(request.responseText);
        }
    });
});

$(document).on("click", "#add_organism", function() {
    var $rows = $("#table_organism");
    var i = $("#table_organism tr:last-child input").data("i") + 1;
    $rows.append(`<tr><td><input list="taxonomic_node_list" name="taxonomic_node_${i}" data-i=${i} placeholder="Ex: Mammalia - [40674; class]" autocomplete="off"/></td></tr>`);
});

$(document).on("click", "#example_1", function() {
    $('#main').load(soft+'form.php #center', function(){
        $("input[name='job_name']").val("example_1");
        $("#spectra_input").hide();
        $("#spectra_input input").attr("required", false);
        $("#spectra_example").show();
        $("input[name='spectra_example_1']").attr("checked", true);
        $('input[name="error_margin_selection"][value="MALDI_TOF"]').attr("checked", true);
        $("#reference_source input").attr("checked", false);
        $("#block_reference_source_default").show();
        $("#block_reference_source_user").hide();
        $("#block_results").show();
        $('input[name="taxonomic_group_selection"][value="birds"]').attr("checked", false);
        $('input[name="taxonomic_group_selection"][value="fishes"]').attr("checked", false);
        $('input[name="taxonomic_group_selection"][value="reptiles"]').attr("checked", false);
    });
}); 

$(document).on("click", "#back_to_normal", function() {
    if ($("input[name='job_name']").val() == "example_1") {
        $("input[name='job_name']").val("");
    }
    $("input[name='spectra_example_1']").attr("checked", false);
    $("#spectra_input").show();
    $("#spectra_input input").attr("required", true);
    $("#spectra_example").hide();
});

function open_taxo_tree(name) {
	window.open(name, 'Taxonomy', 'menubar=no, scrollbars=no, location=no, titlebar=no, status=no, width=800, height=800');
}

$(document).on("click", "a#copy", function () {
    var copyText = $(this).data("copy");
    navigator.clipboard.writeText(copyText);
    alert("Copied the ID: " + copyText);
});