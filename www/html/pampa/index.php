<?php include('header.php') ?>


  <body>
   
   
    <div class="frametitle">
   <h1 id="title">Pampa</h1>                 
   </div>
   
   <div id="center_sup">
     <div class="theme-border" style="display:none"></div>
     <div id="link_home" style="display:inline-block"><a href="/" class="text_onglet"><img src="/Style/icon/home_w.png" alt="home_general"/></a></div>
   <div class="tabs" id="menu_central" style="display:inline-block"><?php include("menu_central.txt")?></div>
   </div>
   <div id="main">
    <div id="center">
     <!--Partie à modifier-->
    <h2>What is Pampa?</h2>
    <p>
      PAMPA (Protein Analysis by Mass Spectrometry for Ancient Species) is a versatile software program specifically designed for ZooMS (Zooarcheological by Mass Spectrometry) and the usage of marker peptides in ZooMS.
    </p>
    <p>
      It can perform a wide range of tasks: conducting taxonomic assignments from mass spectra such as MALDI-TOF or MALDI-FTICR spectra, identifying marker peptides in new species through inference from closely related species, or performing de novo prediction of new markers from the amino-acid sequences alone.  PAMPA is capable of handling any number of  spectra in a single run and enables users to define their own markers. Additionally, PAMPA allows in-depth exploration of various assignment possibilities within the taxonomic space.
    </p>

    <h2>How to use PAMPA ?</h2>
    <p>PAMPA is available in two versions.</p>
    <p><B>Download :</B> the source code for local installation is available at <a href="https://github.com/touzet/pampa" target="_blank">https://github.com/touzet/pampa</a>. This version includes the complete range of features offered by the software.</p>
    <p><B>Web interface :</B> user-friendly web application for fast and easy species identification from mass spectra. It is accessible <a href="/pampa/form.php" class="mc aLoad">here</a>.</p>

    <h2>Authors</h2>
    <p>
    The program has been developed by <a href="https://helene-touzet.cnrs.fr/" target="_blank">Hélène Touzet</a> (CRIStAL) in close collaboration with Fabrice Bray (Lille ZooMS platform).  
    The web interface has been designed by Yohan Hernandez-Courbevoie (CRIStAL), Areski Flissi (CRIStAL) and Hélène Touzet.
    </p>
    
    <h2>Feedbacks</h2>
    <p>
      Please send comments and questions to <a href="mailto:pampa@univ-lille.fr?Subject=[Feedbacks]">PAMPA</a>.
    </p>

      </div><!-- center -->
    </div><!-- main-->

<!-- appel du fichier lib.inc contenant des fonctions php -->
<?php require("../lib.inc")?>

<!-- appel de la fonction footer qui permet d'afficher au bas de la page (nom du logiciel, un lien vers le mail, la date de modif -->
<!-- Modifier le nom Example_web_server par le nom de votre logiciel -->
<?php footer("Pampa","Pampa", "areski.flissi@univ-lille.fr","2024"); ?>
</div>                                                                                

</body>                                        
</html>        
