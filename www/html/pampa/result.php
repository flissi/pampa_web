<?php include('header.php') ?>


  <body>
   
   
    <div class="frametitle">
   <h1 id="title">Pampa</h1>                 
   </div>
   
   <div id="center_sup">
     <div class="theme-border" style="display:none"></div>
     <div id="link_home" style="display:inline-block"><a href="/" class="text_onglet"><img src="/Style/icon/home_w.png" alt="home_general"/></a></div>
   <div class="tabs" id="menu_central" style="display:inline-block"><?php include("menu_central.txt")?></div>
   </div>
   <div id="main">
    <div id="center">
      <br>

     <!--Partie à modifier-->
    <!--h2>Understand the results</h2>
    <p>
        The results are composed of 3 files.
    </p>
    <p>
        More details in the <a>help</a> section.
    </p-->

    <h2>Retrieve analysis results</h2>
    <p>Results remain available for at least one month.</p>
    <p>They can be accessed directly via their link if it has been saved. Otherwise, you can enter your ID in the field below.</p>
    <form id="retrieve" method="post" enctype="multipart/form-data">
        <table class="vide pampa-choice">
          <tr>
            <td>
              <input type="text" name="run_id" maxlength="26" style="width: 300px;"/>
            </td>
            <td>
              <input type="button" id="search" name="button" value="Search"/>
            </td>
          </tr>
        </table>
    </form>
    

      </div><!-- center -->
    </div><!-- main-->

<!-- appel du fichier lib.inc contenant des fonctions php -->
<?php require("../lib.inc")?>

<!-- appel de la fonction footer qui permet d'afficher au bas de la page (nom du logiciel, un lien vers le mail, la date de modif -->
<!-- Modifier le nom Example_web_server par le nom de votre logiciel -->
<?php footer("Pampa","Pampa", "areski.flissi@univ-lille.fr","2024"); ?>
</div>                                                                                

</body>                                        
</html>        
