<?php include('header.php') ?>


<body>
   
   
  <div class="frametitle">
   <h1 id="title">Pampa</h1>                 
  </div>
   
  <div id="center_sup">
     <div class="theme-border" style="display:none"></div>
     <div id="link_home" style="display:inline-block"><a href="/" class="text_onglet"><img src="/Style/icon/home_w.png" alt="home_general"/></a></div>
   <div class="tabs" id="menu_central" style="display:inline-block"><?php include("menu_central.txt")?></div>
  </div>
<div id="main">
  <div id="center">

    <form id="formulaire" method="post" enctype="multipart/form-data">
   
    <input type="hidden" name="pampa_version" value="assign"/>

    <div class="formulaire">
      <table class="vide pampa_choice">
        <tr>
          <td class="label">
            <h2>Name of the job <span style="font-weight: normal">(optional) :</span></h2>
          </td>
          <td>
            <input type="text" name="job_name" maxlength="30"/>
          </td>
        </tr>
      </table>
    </div>
    
    <div class="formulaire">
      <h2>Mass spectra</h2>
      <table class="vide">
        <tr>
          <td class="label"> 
          <B>Upload</B> your MS spectra files in CSV, MGF or mzML format (or in a .ZIP archive)
          </td>
        </tr> 
        <tr id="spectra_input"> 
          <td>
            <input type="file" name="spectra_files" value="file" accept=".csv,.mgf,.mzml,.zip" multiple required/>
          </td>
        </tr> 
        <tr id="spectra_example" style="display: none;">
          <td>
            <input class="file_input" id="back_to_normal" type="button" value="Parcourir..."/>
            pampa_example_1.zip
            <input type="checkbox" style="display: none;" name="spectra_example_1"/>
          </td>
        </tr>
      </table>
      <br>

      <b>Mass error</b>
      <table class="vide pampa_choice">
        <tr>
          <td>
            <input type="radio" name="error_margin_selection" value="MALDI_TOF" checked/>
            Optimize for MALDI-TOF spectra
          </td>
        </tr>
        <tr>
          <td>
            <input type="radio" name="error_margin_selection" value="MALDI_FTICR"/>
            Optimize for MALDI-FTICR spectra
          </td>
        </tr>
        <tr>
          <td>
            <input type="radio" name="error_margin_selection" value="ppm"/>
            Custom value in ppm
          </td>
          <td>
            <input type="number" name="error_margin_ppm" step="1" min="1" max="1000" placeholder="between 1 and 1000"/>
          </td>
        </tr>
        <tr>
          <td>
            <input type="radio" name="error_margin_selection" value="daltons">
            Custom value in Daltons
          </td>
          <td>
            <input type="number" name="error_margin_daltons" step="0.002" min="0.002" max="0.998" placeholder="between 0.002 and 0.998"/>
          </td>
        </tr> 
      </table>
    </div>

    <!--div class="formulaire">
      <h2>Choice of the markers and organisms</h2>
      <table class="vide pampa_choice">
        <tr>
          <td>
            <input type="radio" id="reference_source_default" name="reference_source" value="default"/>
            <B>Basic mode :</B> use PAMPA markers (COL1 peptide markers)
          </td>
        </tr>
        <tr>
          <td>
            <input type="radio" id="reference_source_user" name="reference_source" value="user"/>
            <B>Advanced mode :</B> use your own markers or sequences
          </td>
        </tr>
      </table>
    </div-->

    <table class="vide table-center pampa-choice">
      <tr>
        <td>
          <B>Basic mode </B>
        </td>
        <td>
          <label class="switch" id="reference_source">
            <input type="checkbox" name="reference_source" value="user">
            <span class="slider round"></span>
          </label>
        </td>
        <td>
          <B>Advanced mode </B>
        </td>
      </tr>
    </table>
    <table class="vide table-center pampa-choice">
      <tr>
        <td>
          Advanced mode allows you use your own markers or sequences and taxonomy.
          <p></p>
        </td>
      </tr>
    </table>

    <div id="block_reference_source_default" style="display: block;">
      <div class="formulaire">
        <h2>Organisms and marker peptides</h2>
        <table id="table_organism" class="vide pampa_choice">
          <tr>
            <td class="label" colspan="2">
              <B>Choose</B> one or several taxonomic classes :
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <input type="checkbox" name="taxonomic_group_selection" value="mammals" checked/>
              Mammals
            </td>
          </tr>
          <!--tr>
            <td colspan="2">
              <input type="checkbox" name="taxonomic_group_selection" value="birds" checked/>
              Birds
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <input type="checkbox" name="taxonomic_group_selection" value="fishes" checked/>
              Fishes
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <input type="checkbox" name="taxonomic_group_selection" value="reptiles" checked/>
              Reptiles
            </td>
          </tr-->
          <tr>
            <td class="label" colspan="2">
              or <B>Select</B> a set of organisms :
            </td>
          </tr>
          <tr style="margin-top: 10px;">
            <datalist id="taxonomic_node_list">
              <?php include("datalist_taxo.txt");?>
            </datalist>
            <td>
              <input list="taxonomic_node_list" name="taxonomic_node_1" data-i=1 placeholder="Ex: Mammalia - [40674; class]" autocomplete="off"/>
            </td>
            <td>
              <input type="button" id="add_organism" value="Add organism"/>
            </td>
          </tr>
        </table>

        <br>

        <table class="vide pampa_choice">
          <tr>
            <td>
              <B>PTMs :</B>
            </td>
            <td>
              <input type="checkbox" name="ptm_deamidation" value="deamidation" checked/>
              Include deamidations
            </td>
          </tr>
        </table>
      </div>
    </div>

    <div id="block_reference_source_user" style="display: none;">
      <div class="formulaire">
        <h2>Organisms and peptide markers</h2>
        <table class="vide pampa_choice">
          <tr>
            <td>
              Upload one or several peptide table(s) 
            </td>
            <td>
              <input type="file" name="peptides_files" value="file" accept=".tsv,.zip" multiple/>
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <B>or</B>
            </td>
          </tr>
          <tr>
            <td>
              Upload a set of protein sequences for in silico digestion 
            </td>
            <td>
              <input type="file" name="sequences_files" value="file" accept=".fa,.fasta,.fna,.zip" multiple/>
            </td>
          </tr>
        </table>
      </div>

      <div class="formulaire">
        <h2>Taxonomy</h2>
        <table class="vide">
          <tr>
            <td colspan="2">
              <input type="radio" name="taxonomy_selection" value="default" checked/>
              Use NCBI taxonomy
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <input type="radio" name="taxonomy_selection" value="no"/>
              Provide no taxonomy
            </td>
          </tr>
          <tr>
            <td>
              <input type="radio" name="taxonomy_selection" value="custom"/>
              Upload your own taxonomy 
            </td>
            <td>
              <input type="file" name="taxo_file" value="file" accept=".tsv"/>
            </td>
          </tr>
        </table>
      </div>
    </div>

    <div id="block_results">
      <div class="formulaire">
        <h2>Results</h2>
        <table class="vide">
          <tr>
            <td colspan="2"> 
              <input type="radio" name="nearoptimal_selection" value="optimal" checked/>
              Only optimal results
            </td>
          </tr>
          <tr>
            <td> 
              <input type="radio" name="nearoptimal_selection" value="nearoptimal"/>
              Near-optimal results within a suboptimality percentage :
            </td>
            <td>
              <input type="number" name="nearoptimal_margin" placeholder="Value in range 0..100" min="0" max="100" step="1"/>
            </td>
          </tr>
          <tr>
            <td> 
              <input type="radio" name="nearoptimal_selection" value="nearoptimal_all"/>
              All results within a suboptimality percentage :
            </td>
            <td>
              <input type="number" name="nearoptimal_all_margin" placeholder="Value in range 0..100" min="0" max="100" step="1"/>
            </td>
          </tr>
        </table>
      </div>
    </div>

    <div class="center">
      <input type="button" id="example_1" name="example_1" value="Example">
    </div>

    <div class="center">
      <input type="submit" id="reset" name="reset" value="Reset" /> 
      <input type="submit" id="run" name="button" value="Run" />
      <input type="hidden" name="command" value="request" /> 
    </div>

  </form>

</div><!--bloc -->
</div><!-- main-->

<!-- chargement de la librairie php lib.inc -->
   <?php require("../lib.inc")?>
<!-- appel de la fonction footer qui permet d'afficher au bas de la page (nom du logiciel, un lien vers le mail, la date de modif -->
<!-- A modifier en fonction de votre logiciel -->
   <?php footer("Pampa","Pampa", "areski.flissi@univ-lille.fr","2024"); ?>
                                                                                


</body>                                        
</html>        

