<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link style="text/css" rel="stylesheet" href="/Style/css/bioinfo.css" />
    <link style="text/css" rel="stylesheet" href="/Style/css/page_theme.css" />
    <!--modifier le nom du dossier et du fichier css ci dessous, mettre le même nom que vous lui avez donné -->
    <link href="/pampa/pampa.css" rel="stylesheet" style="text/css"/>
    <script type="text/javascript" src="/libs/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="/libs/jquery.history.js"></script>
    <script type="text/javascript" src="/scripts/bioinfo.js"></script>
    <!--modifier le nom du dossier ci dessous -->
    <script type="text/javascript" src="/pampa/js/script.js"></script>   
   <title>Bonsai  :: Bioinformatics Software Server</title>
    <script type="text/javascript">
        var i_am_old_ie = false;
    </script>
    <!--[if LT IE  9]>
    <script type="text/javascript">
            i_am_old_ie = true;
    </script>
    <![endif]-->
  </head>
  <body>
   
   
    <div class="frametitle">
   <h1 id="title">Pampa light</h1>                 
   </div>
   
   <div id="center_sup">
     <div class="theme-border" style="display:none"></div>
     <div id="link_home" style="display:inline-block"><a href="/" class="text_onglet"><img src="/Style/icon/home_w.png" alt="home_general"/></a></div>
   <div class="tabs" id="menu_central" style="display:inline-block"><?php include("menu_central.txt")?></div>
   </div>
    <div id="main">
     <div id="center">
<form id="formulaire" method="post" enctype="multipart/form-data">

	<input type="hidden" name="pampa_version" value="light"/>

  <!--div class="formulaire">
    <table class="vide">
      <tr>
	<td class="label">Enter a <b>name</b> for the sequences 
	  <i>(optional) </i> : 
	  <input id="seq_name" type="text" name="seq_name" size="20" />
	</td>
      </tr>
    </table>
  </div-->
  <!-- Partie à adapter en fonction de votre programme, essayer d'avoir des noms (name) et id (id) en adéquation avec ce que vous demandez  -->
   

	<h2>Spectra</h2>
        <div class="formulaire">
                <table class="vide">
                <tr>
                <td class="label"> 
                <B>Upload</B> your MS spectra files in CSV format
                </td>
                </tr> 
                <tr> 
                <td>
                <input type="file" name="spectra_files" value="file" accept="csv" multiple></input>
                </td>
                </tr> 
                </table>
        </div>



    <h2>Error margin</h2>
    <div class="formulaire">
    <table class="vide">
      <tr>
	<td class="label"> 
	  Error margin can be expressed in Daltons (recommended values are 0.1 for maldi TOF, and 0.01 for maldi FT) or in ppm (recommended values are 50 for maldi TOF, and 5 for maldi FTICR)
	</td>
      </tr>
	<tr>
		<td>
		<input type="number" name="error_margin" placeholder="Ex. 0.1 (Da) or 50 (ppm) " step="0.01"></input>
		</td>
	</tr> 
      <!--tr>
	<td><input id="ex1" type="submit" name="example" value="Example" /></td>
      </tr-->
    </table>
    </div>

	<!--h2>Taxonomy</h2>
    	<div class="formulaire">
    		<table class="vide">
      		<tr>
        	<td class="label"> 
          	<B>Upload</B> the taxonomy file TSV format
        	</td>
      		</tr> 
        	<tr> 
                <td>
                <input type="file" name="taxo_file" value="file" accept="tsv"></input>
                </td>
        	</tr> 
		</table>
	</div-->

    <!--div class="formulaire">
    <table class="vide">
      <tr>
	<td class="label">
	  <input id="GC" type="checkbox" name="GC" value="pourcentage GC" />
	  Disable threshold correction according to GC%
	</td>
      </tr>
    </table>
  </div-->

  <!--div class="formulaire">
    <p>
    Select your case :
    <select id="case" name="case">
        <option value="U">Uppercase</option>
        <option value="L">Lowercase</option>
      </select>
    </p>
    
  </div-->

  
  <!--div class="formulaire">
    <table class="vide">
      <tr>
	<td class="label"> 
	  Enter your <b>E-mail</b> address <i>(optional)</i>: 
	  <input id="email" type="text" name="email" size="20" />
	</td>
      </tr>
    </table>
  </div-->

  <div class="center">
    <input type="submit" id="reset" name="reset" value="Reset" /> 
    <input type="submit" id="run" name="button" value="Run" />
    <input type="hidden" name="command" value="request" /> 
  </div>

</form>

</div><!--bloc -->
</div><!-- main-->

<!-- chargement de la librairie php lib.inc -->
   <?php require("../lib.inc")?>
<!-- appel de la fonction footer qui permet d'afficher au bas de la page (nom du logiciel, un lien vers le mail, la date de modif -->
<!-- A modifier en fonction de votre logiciel -->
   <?php footer("Pampa","Pampa", "areski.flissi@univ-lille.fr","2023"); ?>
                                                                                


</body>                                        
</html>        

