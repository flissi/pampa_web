import cgitb; cgitb.enable()
import os, sys
import csv
import time

from html2image import Html2Image
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from PIL import Image
#from Screenshot import Screenshot_Clipping

import gen_subtaxo

def gen_sub_tree(chemin_result, run_id, assign_name, assignment, color, url_json):
    
    assignment_file = open(chemin_result + run_id + "/" + assign_name +".php", "a")


    assignment_file.write("<script type='text/javascript' src='/libs/jquery-1.11.0.js'></script>")
    assignment_file.write("<link rel='stylesheet' href='/pampa/js/jstree/style.min.css' />")
    assignment_file.write("<script src='/pampa/js/jstree/jstree.min.js'></script>")
    assignment_file.write("<link style='text/css' rel='stylesheet' href='/Style/css/bioinfo.css'>")

    # url_json = "/pampa/result/" + run_id + "/" + taxo_corename + ".json"
    # url_json = "/pampa/data_pampa/taxonomy_reduced.json"

    jstree_function = """
                <script type="text/javascript">         
                        $(function() {
                                $('#taxo').jstree({
                                        'core' : {
                                                "multiple" : true,
                                             'data' : {
    """
    jstree_function += "'url' : '"
    jstree_function += url_json
    jstree_function += "'"


    jstree_function += """
                                             }
                                           }
                                });
                                $('#taxo').bind("loaded.jstree", function () {
                                        $('#taxo').jstree('close_all');
    """





 
    spectra = assignment["spectrum_name"]
    summary = ""
    score = assignment["score"]
    rank = assignment["lca_rank"]
    assignId = assignment["lca"]
    assign = assignment["lca_name"]



    jstree_function += "$('#taxo').jstree('enable_node', " + assignId + ");"
    jstree_function += "$('#taxo').jstree('select_node', " + assignId + ");"
    jstree_function += "$('#taxo').jstree('set_text', " + assignId + ",' <font color=darkgrey>[" + spectra + "] &#8594;</font> <i style=color:darkgrey>" + assign +  "</i> ' );"


    for taxon in assignment["taxa"]:
        taxoid = taxon["id"]
        name = taxon["name"]
        jstree_function += "$('#taxo').jstree('enable_node', " + taxoid + ");"
        jstree_function += "$('#taxo').jstree('select_node', " + taxoid + ");"
        jstree_function += "$('#taxo').jstree('set_text', " + taxoid + ",' <i style=color:" + color +">" + name + " &check;</i>');"
        species = "<i><u>"+ name + "</u></i> <font color='" + color +"'>&check;</font>, "


    jstree_function += "     });"
    jstree_function += "});"
    jstree_function += "</script>"

    #assignment_file.write("<hr>")
    assignment_file.write("<br/>")
    assignment_file.write("<div id='taxo'></div>")

    assignment_file.write(jstree_function)


    assignment_file.close()

    options = Options()
    options.add_argument('--no-sandbox')
    options.add_argument("--headless")
    options.add_argument('--disable-dev-shm-usage')
    options.add_argument('--window-size=1500x1200')
    driver = webdriver.Chrome(options)


    driver.get('http://localhost/pampa/result/' + run_id + '/' + assign_name + '.php')

    try:
        elt = WebDriverWait(driver, 30).until(EC.presence_of_element_located((By.ID, assignId + "_anchor")))
    finally:
        driver.save_screenshot(chemin_result + run_id + "/" + assign_name +".png")

    driver.quit()
