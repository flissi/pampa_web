#!/usr/bin/python
# -*- coding: utf-8 -*-

# Modules import
import cgitb; cgitb.enable()
import shutil
import os
import cgi
import common
import sys
import json
import re
import pandas as pd
import numpy as np
from bokeh.plotting import figure, show, output_file
from bokeh.models import ColumnDataSource, NumeralTickFormatter
from bs4 import BeautifulSoup
from pyteomics import mzml, mgf
import operator

from html_utils import *
from render_sub_assignment import *

# Functions definition
def peak_parser_csv(peak_file_name):
    """Parse les spectres CSV. Parser copié à partir du code de PAMPA."""
    peak_list = []    
    f = open(peak_file_name)
    next(f) 
    for row in f:
        peak = re.split(',|;', row)
        if len(peak)>1:
            peak_list.append((float(peak[0]), int(float(peak[1]))))
        elif len(peak)==1:
            peak_list.append((float(peak[0]), 0))
   
    return pd.DataFrame(peak_list, columns=("mz", "I"))


def peak_parser_mgf(peak_file_name):
    """Parse les spectres MGF. Parser copié à partir du code de PAMPA."""
    peak_list = []
    
    f = mgf.read(peak_file_name)
    mass_array = f[0]["m/z array"] #pour avoir les m/z des pics
    intensity_array = f[0]["intensity array"] #pour avoir l'intensité des pics

    for mass, intensity in zip(mass_array.tolist(), intensity_array.tolist()):
        peak = (float(mass), int(float(intensity)))
        if peak not in peak_list:
            peak_list.append(peak)
            
    return pd.DataFrame(peak_list, columns=("mz", "I"))


def peak_parser_mzml(peak_file_name):
    """Parse les spectres MZML. Parser copié à partir du code de PAMPA."""
    peak_list = []
    f = mzml.MzML(peak_file_name, use_index=True) 
    mass_array = f[0]["m/z array"] #pour avoir les m/z des pics
    intensity_array = f[0]["intensity array"] #pour avoir l'intensité des pics
 
    for mass, intensity in zip(mass_array.tolist(), intensity_array.tolist()):
        couple = (float(mass), float(intensity))
        if couple not in peak_list:
            peak_list.append(couple)
          
    return  pd.DataFrame(peak_list, columns=("mz", "I"))


def spectrum_parser(file_path):
    """Redirige vers les différents parsers de spectres."""
    _, ext = os.path.splitext(file_path)
    if ext == ".csv" or ext == ".CSV" or ext == ".txt" or ext == ".TXT": # csv file 
        spectrum = peak_parser_csv(file_path)
    elif ext == ".mgf" or ext == ".MGF" : # mgf file 
        spectrum = peak_parser_mgf(file_path)
    elif ext == ".mzML": # mzML file
        spectrum = peak_parser_mzml(file_path)
    else:
        pass # Situation gérée par PAMPA
    return spectrum

def draw_spectrum_bokeh(resdir, spectrum_name, peaks_mass_name, color_hexa="#FF0000", decimals=5):
    """Construit le script js/HTML permettant la visualisation du spectre."""
    # Fetch data
    df = spectrum_parser(resdir+'spectra/'+spectrum_name)
    df.columns = ["mz", 'I']

    # Transform data display peaks with a line instead of bars
    # + round mass values
    # + add annotation
    # + add signal 'assigned' or 'unknown'
    mzTransformed = [ x for y in [(x, x, x) for x in [round(x, decimals) for x in df['mz'] ]] for x in y ]
    intensTransformed = [ x for y in [ (0, x, 0) for x in df['I'] ] for x in y ]
    assigned = pd.DataFrame([(round(x, decimals), y) for (x, y) in peaks_mass_name], columns=["mass", "name"])
    df2 = pd.DataFrame({'mz': mzTransformed, 'I': intensTransformed}).join(assigned.set_index('mass'), on='mz').fillna("--")
    df3 = pd.concat([df2, df2["name"].where(df2["name"] == "--", "assigned").replace("--", "unknown").to_frame("signal")], axis=1)
    
    # Output as HTML file
    output_file(f'{resdir}{spectrum_name}.html')
    
    # Create figure
    cds = ColumnDataSource(data=df3)
    tooltips = [
        ('m/z','@mz{0.00000}'),
        ('Intensity','@I'),
        ('Name', '@name')
        ]
    p = figure(
        width=800, height=300,
        title = '',
        tools = 'save,xwheel_zoom,xpan,box_zoom,undo,reset',
        tooltips=tooltips
        )
    
    # Prep. for 'signal' annotation ('assigned' or 'unknown')
    sources = []
    for idx, cat in enumerate( ("unknown", "assigned") ):
        sources.append((idx, cat, ColumnDataSource(data=df3[df3['signal'] == cat])))

    colors = ("#000000", color_hexa)
    for idxColor, cat, cds in sources:
        idxColor = idxColor % 10
        p.line(
            'mz', 'I',
            source = cds,
            color = colors[idxColor],
            line_width = 2, alpha = 0.7,
            legend_label=cat
        )
    p.line(
        x = [ df3['mz'].min(), df3['mz'].max() ],
        y = [0, 0],
        color = colors[0],
        line_width = 2
    )
    
    # Legend config
    p.legend.location = 'top_right'
    #Click on the legend item and the corresponding line will become hidden
    p.legend.click_policy = 'hide'
    p.legend.title = 'Signal Type'

    #Format axis labels
    p.xaxis.axis_label = 'Fragment m/z'
    p.xaxis.axis_label_text_font_size = '10pt'
    p.xaxis.major_label_text_font_size = '9pt'

    p.yaxis.axis_label = 'Intensity'
    p.yaxis.axis_label_text_font_size = '10pt'
    p.yaxis.major_label_text_font_size = '9pt'
    p.yaxis.formatter = NumeralTickFormatter(format='0.')

    show(p)


def insert_spectrum(resdir, spectrum_name):
    """Extrait le <div> de visualisation du spectre du fichier généré par Bokeh."""
    html = ""
    html += ('<div class="block_spectrum">')
    soup = BeautifulSoup(open(resdir+spectrum_name+'.html', 'r'), 'html.parser')
    for element in soup.select('body div, body script'):
        html += (str(element))
    html += ('</div>')
    return html



def write_details_page(spectrum_name, run_id, taxo_info, job_name=None):
    """
    Écrit la page de détails.
    Cette page existe en deux versions : une pour les assignations simples et une autre pour les assignations multiples.
    La taxonomie peut être affichée, ou non, en fonction de la valeur contenue dans "taxo_info" (default, custom ou no).
    """

    # Récupération du json de taxonomie en fonction de la valeur de 'taxo_info'
    resdir = f"{common.RESULT_DIR}{run_id}/"
    if taxo_info == "custom":
        url_json = f"/pampa/result/{run_id}/taxonomy_{run_id}.json"
    elif taxo_info == "default":
        url_json = "/pampa/data_pampa/taxonomy_reduced.json"
    else:
        pass # taxo_info=="no" pas d'affichage de l'arbre

    # Définition des variables
    content = json.load(open(resdir+"out_"+run_id+".json", "r"))
    content_reduced = [dct for dct in content if dct['spectrum_name'] == spectrum_name]
    nb_assignments = len(content_reduced)
    list_of_colors = ["#0000FF", "#FF0000", "#00FF00", "#FF00FF", "#0080FF", "#FF8000", "#6495ED", "#66CDAA", "#7FFF00", "#66CDAA", "#008000", "#800080"]
    decimals = 5

    html = ""
    
    # Insert HTML header and page head
    html += (open(f"{common.HTML_PATH}/header.php", "r").read())
    html += '<script> console.log(performance.navigation.type) </script>'
    html += '<div class="frametitle"><h1 id="title">Pampa</h1></div><div id="center_sup"><div class="theme-border" style="display:none;"></div><div id="link_home" style="display:inline-block"><a href="/" class="text_onglet"><img src="/Style/icon/home_w.png" alt="home_general"/></a></div><div class="tabs" id="menu_central" style="display:inline-block">'
    html += open(f"{common.HTML_PATH}/menu_central.txt", 'r').read()
    html += '</div></div><div id="main"><div id="center">'

    # Début de la page
    html += '<div id="details">'
    html += (f'<h2>Results for job {run_id} {f"({job_name})" if job_name is not None else ""}<br>Mass spectrum {spectrum_name}</h2>')
    
    if nb_assignments == 0:
        # pas d'assignation
        html += ('<p>No assignment have been found for this spectrum.</p>')

    elif nb_assignments == 1:
        # assignation simple
        assignment = content_reduced[0]
        color = list_of_colors[0]
        
        # Head
        html += (f'<p>One assignment is proposed (from the analysis of the {assignment["spectrum_length"]} peaks). The results are summarized below.</p>')
        
        # General info
        html += ('<ul>')
        if assignment["lca_rank"] is not None:
            html += (f'<li>Taxonomic group : {assignment["lca_name"]} ({assignment["lca_rank"]}), with P-value {assignment["pvalue"]:.2E}</li>')
        matching_species = ', '.join((f"{k['name']} ({k['id']})" for k in assignment['taxa']))
        html += (f'<li>Matching species : {matching_species}</li>')
        if assignment["hca_rank"] is not None:
            html += (f'<li>Uncertainty : {assignment["hca_name"]} ({assignment["hca_rank"]})</li>')
        else:
            html += f'<li>P-value : {assignment["pvalue"]:.2E}</li>'
        html += ('</ul>')
        html += ('<br>')

        # Taxonomy
        if taxo_info != "no" :
            assignment_png = assignment["lca_name"].replace(' ', '_') + ".png"
            assignment_tree = assignment["lca_name"].replace(' ', '_') + ".php"
            html += '<p><iframe src="' + assignment_tree + '" height="800px" width="90%"></iframe></p>'
            html +='<br/><br/>'
            html += (f'<div align="center"><a href="{assignment_png}" download>Download PNG</a></div>')
            html += '<br/>'



        # Spectra
        peaks_mass_name = []
        for peak_info in assignment['peaks']:
            peaks_mass_name.append((peak_info["mass"], f'{peak_info["code"]} - {peak_info["PTM"]}'))
        draw_spectrum_bokeh(resdir, spectrum_name, peaks_mass_name, color_hexa=color, decimals=decimals)
        html += insert_spectrum(resdir, spectrum_name)
        

        # Peaks assigned table
        html += ('<h4>Relation between peaks and markers</h4>')
        html += ('<table id="detail_peaks_pampa" class="result">')
        header = ["Marker", "m/z", "Intensity", "Protein", "Begin", "End", "Sequence"]
        html += (f'<tr>{"".join((f"<th>{e}</th>" for e in header))}</tr>')
        peaks_info = sorted(assignment['peaks'], key=lambda x: operator.itemgetter("code", "PTM")(x))
        for peak_info in peaks_info:
            row = [
                f'{peak_info["code"]} - {peak_info["PTM"]}',
                round(peak_info["mass"], decimals),
                int(float(peak_info["intensity"])), 
                peak_info["protein"],
                peak_info["begin"],
                peak_info["end"]
            ]
            html += ('<tr class="impair">')
            html += ("".join((f'<td>{e}</td>' for e in row)))
            html += f'<td>{peak_info["sequence"]}</td>'
            html += ('</tr>')
        html += ('</table>')
        html += ('<br>')

        # Sequences (alignment) - optionnal

    
    else:
        # assignations multiple
        # Head
        html += (f'<p><B>{nb_assignments} assignments</B> are proposed (from the analysis of the {content_reduced[0]["spectrum_length"]} peaks). The results are summarized below.</p>')

        content_reduced.sort(key=lambda x: x["pvalue"])
        peak_info_dict = {}

        if taxo_info != "no" :
            assignment_png = spectrum_name + ".png"
            assignment_tree = spectrum_name + ".php"
            html += '<p><iframe src="' + assignment_tree + '" height="850px" width="90%"></iframe></p>'
            html += '<br/><br/>'
            html += (f'<div align="center"><a href="{assignment_png}" download>Download PNG</a></div>')
            html += '<br/>'

        for i in range(nb_assignments):
            assignment = content_reduced[i]
            color = list_of_colors[i%12]
            peaks_mass_name = []
            for peak_info in assignment['peaks']:
                peaks_mass_name.append((peak_info["mass"], f'{peak_info["code"]} - {peak_info["PTM"]}'))
                key = f"{peak_info['code']} - {peak_info['PTM']}|{round(peak_info['mass'], decimals)}|{int(peak_info['intensity'])}"
                if key not in peak_info_dict:
                    peak_info_dict[key] = [i]
                else:
                    peak_info_dict[key].append(i)

            # build semi-global taxonomic tree for multi-assignation
            if taxo_info != "no":
                gen_sub_tree(common.RESULT_DIR, run_id, spectrum_name, assignment, color, url_json)


            # General info
            html += '<br>'
            html += (f'<h3><span class="carre_vide" style="border-color: {color};"></span> Assignment {i+1}</h3>')
            html += ('<ul>')
            if assignment["lca_rank"] is not None:
                html += (f'<li>Taxonomic group : {assignment["lca_name"]} ({assignment["lca_rank"]}), with P-value {assignment["pvalue"]:.2E}</li>')
            matching_species = ', '.join((f"{k['name']} ({k['id']})" for k in assignment['taxa']))
            html += (f'<li>Matching species : {matching_species}</li>')
            if assignment["hca_rank"] is not None:
                html += (f'<li>Uncertainty : {assignment["hca_name"]} ({assignment["hca_rank"]})</li>')
            else:
                html += f'<li>P-value : {assignment["pvalue"]:.2E}</li>'
            html += ('</ul>')
            html += '<br>'

            # Spectra
            draw_spectrum_bokeh(resdir, spectrum_name, peaks_mass_name, color_hexa=color, decimals=decimals)
            html += insert_spectrum(resdir, spectrum_name)


            # Taxonomy
            #html += '<h4>Taxonomy</h4>'
            assignment_png = assignment["lca_name"].replace(' ', '_') + ".png"
            assignment_tree = assignment["lca_name"].replace(' ', '_') + ".php"
            #html += href(assignment_tree,'<img src="' + assignment_png + '" width="95%"/>')
            #html += '<iframe src="' + assignment_tree + '" height="600px" width="90%"></iframe>'
            #html += '<br/><br/>'
            #html += (f'<div align="center"><a href="{assignment_png}" download>Download PNG</a></div>')
            #html += '<br/>'

             # Peaks assigned table
            html += ('<h4>Relation between peaks and markers</h4>')
            html += ('<table id="detail_peaks_pampa" class="result">')
            header = ["Marker", "m/z", "Intensity", "Protein", "Begin", "End", "Sequence"]
            html += (f'<tr>{"".join((f"<th>{e}</th>" for e in header))}</tr>')
            for peak_info in assignment['peaks']:
                row = [
                    f'{peak_info["code"]} - {peak_info["PTM"]}',
                    round(peak_info["mass"], decimals),
                    int(float(peak_info["intensity"])), 
                    peak_info["protein"],
                    peak_info["begin"],
                    peak_info["end"]
                ]
                html += ('<tr class="impair">')
                html += ("".join((f'<td >{e}</td>' for e in row)))
                html += f'<td>{peak_info["sequence"]}</td>'
                html += ('</tr>')
            html += ('</table>')
            html += ('<br>')

            # Sequences (alignment) - optionnal

        # Overview (recap)
        html += '<br><br>'
        html += ('<h3>Distribution of peptide markers across assignments</h3>')
        html += ('<table id="detail_summary_pampa" class="result">')
        header = ["Marker", "m/z", "Intensity"] + [f"#{i}" for i in range(1, nb_assignments+1)]
        html += (f'<tr>{"".join((f"<th>{e}</th>" for e in header))}</tr>')
        keys_sorted = sorted(list(peak_info_dict.keys()), key=lambda x: x.split('|')[0])
        for key in keys_sorted:
            html += ('<tr class="impair">')
            html += ("".join((f'<td>{e}</td>' for e in key.split('|'))))
            html += ("".join(('<td>▪</td>' if i in peak_info_dict[key] else '<td style="padding-left: 15px; padding-right: 15px;"></td>' for i in range(nb_assignments))))
            html += ('</tr>') 
        html += ('</table>')
        html += ('<br>')

    
    html += ('<br>')
    html += ('</div>')
    html += ('<br>')
    html += ('<br>')
    html += ('</div>')
    html += '<script> console.log("details loaded")</script>'
    html += '<script> if (performance.navigation.type == 2 || performance.navigation.type == 0) {location.reload(true);}</script>'
    
    # Insert page footer
    html += '</div></div>'
    html += open(f"{common.HTML_PATH}/footer.php", "r").read()
    html +='</body></html>'
    
    # Write details page
    open(f'{common.RESULT_DIR}{run_id}/details_{os.path.splitext(spectrum_name)[0].replace(" ", "_")}.html', "w").write(html)


def extract_request(form):
    error = 0
    error_messages = []
    url_result = ""

    if "run_id" in form:
        run_id = form["run_id"].value
    else:
        error = 1
        error_messages.append("The script need the parameter 'run_id'.")
        run_id = ""

    if "spectrum_name" in form:
        spectrum_name = form["spectrum_name"].value
    else:
        error = 1
        error_messages.append("The script need the parameter 'spectrum_name'.")
        spectrum_name = ""

    if "job_name" in form:
        job_name = form["job_name"].value
        if job_name == "None":
            job_name = None
    else:
        error = 1
        error_messages.append("The script need the parameter 'job_name'.")

    if "taxo" in form:
        taxo_info = form["taxo"].value
    else:
        error = 1
        error_messages.append("The script need the parameter 'taxo'.")

    if error == 0:
        write_details_page(spectrum_name, run_id, taxo_info, job_name)

    return error, error_messages, run_id, spectrum_name
    



# Main
def main():
    """Génération de la page de détails (pour une assignation, simple ou multiple)"""
    
    form = cgi.FieldStorage()

    error, error_messages, run_id, spectrum_name = extract_request(form)

    sys.stdout.write("Content-Type: application/json")
    sys.stdout.write("\n")
    sys.stdout.write("\n")
    result={}
    if error == 0:
        result['success'] = True
        result['run_id'] = run_id
        result['url_result'] = f'/pampa/result/{run_id}/details_{os.path.splitext(spectrum_name)[0].replace(" ", "_")}.html'
    else:
        result['success'] = False
        result['error_messages'] = error_messages
    sys.stdout.write(json.dumps(result, indent=1))
    sys.stdout.write("\n")

main()
