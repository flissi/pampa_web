import cgitb; cgitb.enable()
import os, sys
from html_utils import *
import common




def write_main_page(run_id):
    """Write a HTML page. This result page display errors made by the user when filling the formular."""
    main_page = open(common.RESULT_DIR + run_id + "/results.php", "w")
    
    # Insert page header
    main_page.write(open(f"{common.HTML_PATH}/header.php", "r").read())
    main_page.write('<div class="frametitle"><h1 id="title">Pampa</h1></div><div id="center_sup"><div class="theme-border" style="display:none"></div><div id="link_home" style="display:inline-block"><a href="/" class="text_onglet"><img src="/Style/icon/home_w.png" alt="home_general"/></a></div><div class="tabs" id="menu_central" style="display:inline-block">')
    main_page.write(open(f"{common.HTML_PATH}/menu_central.txt", 'r').read())
    main_page.write('</div></div><div id="main"><div id="center">')

    # Show the error messages
    main_page.write("<div class='formulaire'>")
    main_page.write("<h2>The analysis has been interrupted because of the following issue(s) in PAMPA</h2>")
    main_page.write(ul("".join([li(error_msg) for error_msg in open(common.RESULT_DIR + run_id + "/error.log", "r").read().split("\n") if error_msg != ""])))
    main_page.write("</div>")
    main_page.write("<br/>")

    # Back to the formular
    main_page.write('<a href="/pampa/form.php" class="aLoad" >Back to PAMPA classify</a>')

    # Insert page footer
    main_page.write('</div></div>')  # center - main
    main_page.write(open(f"{common.HTML_PATH}/footer.php", "r").read())
    main_page.write('</body></html>')

    main_page.close()


# Main program
def main():
    
    """
    Cette page affiche les erreurs rencontrées par PAMPA lors de son analyse. Ces erreurs ont été écrite dans le fichier 'error.log' dans le dossier de résultats de l'analyse, chaque ligne contenant une erreur rencontrée par PAMPA.

    Les erreurs mineures n'affectant pas le fait que PAMPA termine son analyse sont renseignées dans le fichier de sortie 'warning.log' et affichées directement sur la page de résultats.
    """

    # Parse the parameters and set variables
    run_id = sys.argv[1]

    write_main_page(run_id)

main()