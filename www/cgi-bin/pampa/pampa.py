#!/usr/bin/python
# -*- coding: utf-8 -*-
import cgitb; cgitb.enable()
import os
import cgi
import json
import re
import sys
import common
import shutil
import zipfile


#Vérification du mail
def verif_mail(mail):
    """ check the validity of email address """

    res = mail.strip()
    if res.find(' ') > 0:
        return False
    a = res.find('@')
    if a <= 0:
        return False
    if res.find('@', a+1) > 0:
        return False
    return True

def extract_files_from_fileitems(fileitems, final_dir, extensions_list=None, unzip=False):
    """
    Cette fonction permet l'extraction de fichiers et leur copie vers le répertoire de résultats de l'analyse (dossier 'result/{run_id}').
    Ces fichiers ont été fournie par l'utilisateur, le paramètre 'fileitems' dont correspondre à un objet fileitems (type spécifique associé au téléchargement de fichiers). Ils serviront dans l'analyse de PAMPA.
    Cette fonction permet de faire un filtre entre les fichiers dont l'extension est valide (extensions passés en arguments).
    Les fichiers peuvent être contenus dans un fichier ZIP.
    """
    files_moved = []
    files_not_moved = []

    final_dir = final_dir.rstrip('/')
    if not os.path.exists(final_dir):
        os.mkdir(final_dir)

    if extensions_list:
        extensions = "|".join(extensions_list)

    for f in fileitems:
        fn = os.path.basename(f.filename)
        re_extension = re.compile(os.path.splitext(fn)[1], re.IGNORECASE)  # for extension check
        if unzip and re_extension.search(".zip"):
            with zipfile.ZipFile(f.file, "r") as myzip:
                for element in myzip.filelist:
                    fname = element.filename 
                    re_extension = re.compile(os.path.splitext(fname)[1], re.IGNORECASE)  # for extension check
                    if (extensions_list is None or re_extension.search(extensions)) and not os.path.exists(final_dir + '/' + fname):
                        open(final_dir + '/' + fname, 'wb').write(myzip.open(fname, "r").read())  # cp file
                        files_moved.append(fname)
                    elif fname[-1] == "/":
                        pass  # if a directory is in the zipfile.filelist: pass (no recursivity)
                    else:
                        files_not_moved.append(fname)
        elif (extensions_list is None or re_extension.search(extensions)) and not os.path.exists(final_dir + '/' + fn):
            open(final_dir + '/' + fn, 'wb').write(f.file.read())  # cp file
            files_moved.append(fn)
        else:
            files_not_moved.append(fn)
    
    return files_moved, files_not_moved

def extract_files_from_list(filename_list, final_dir):
    """
    Déplacement de fichiers internes.
    l'unzip est systématique
    """
    final_dir = final_dir.rstrip('/')
    if not os.path.exists(final_dir):
        os.mkdir(final_dir)

    for fn in filename_list:
        if os.path.exists(fn):
            re_extension = re.compile(os.path.splitext(fn)[1], re.IGNORECASE)  # to detect ZIP archive
            if re_extension.search(".zip"):
                with zipfile.ZipFile(fn, "r") as myzip:
                    for element in myzip.filelist:
                        fname = element.filename 
                        if fname[-1] != "/":  # not a directory
                            open(final_dir + '/' + fname, 'wb').write(myzip.open(fname, "r").read())  # cp file
            else:
                open(final_dir + '/' + os.path.basename(fn), 'w').write(open(fn, "r").read())  # cp file
    return None



#Fonction permettant de vérifier les données soumises dans le formulaire, retour dans un dictionnaire (req)
#Fonction qui sera modifiée pour le traitement de vos données
def extract_request(form):
    """ check the validity of the different fields of form 
    
    Cette section vérifie la validité des argument soumis et les conditionne afin de lancer PAMPA comme souhaité.
    """
    error_found = False
    error_messages = []
    req = {}
    
    # fichier de log où les points importants du traitement de la requête seront incris
    log = open('log.txt', 'w')
    log.write(f'Starting log file\n')  # log
    
    # Sauvegarde :
        #       - fichiers CSV spectra dans le dossier result/run_id/spectra/
        #       - fichiers FASTA sequences dans le dossier result/run_id/sequences/
        #       - fichier TSV peptides_table dans result/run_id/
        #       - fichier TSV taxonomy (optionnel ?) dans result/run_id/

        # Nom du fichier output fixé a partir du run_id 

        # Resolution par defaut 0.01 ? 

    # dossier de résultats utilisateur
    resdir = common.RESULT_DIR + form['run_id'].value
    log.write(f"Result directory : {resdir}\n")

    # table des peptides par défaut
    # attention: certains de ces chemins (path) renvoient vers des fichiers qui n'existent peut-être pas encore par manque de données (ex: birds, reptiles, fish)
    peptides_path_dict = {"mammals": common.PEPTIDES_MAMMALS_FILE, "birds": common.PEPTIDES_BIRDS_FILE, "fishes": common.PEPTIDES_FISHES_FILE, "reptiles": common.PEPTIDES_REPTILES_FILE}
    
    req['files_uploaded'] = []  # for the render_result
    
    #req['pampa_version'] = form['pampa_version']

    # Job name parsing (optionnal)
    if "job_name" in form:
        job_name = form["job_name"].value.strip().replace(' ', '_')
        if job_name == "":
            log.write("Job name: None\n")  # log
        elif len(job_name) <= 30:
            req['job_name'] = job_name
            log.write(f"Job name: {job_name}\n")  # log
        else:
            error_messages.append(f"The job name cannot exceed 30 characters. Current size: {len(job_name)}.")
    else:
        log.write("Job name: None\n")
    
    # Spectra files parsing
    rel_spectra_dir = "spectra"
    abs_spectra_dir = os.path.join(resdir,rel_spectra_dir)  # destination directory
    req['spectra_dir'] = abs_spectra_dir  # for the command
    log.write(f"spectra_dir: {req['spectra_dir']}\n")  # log

    if 'spectra_example_1' in form:
        # Cas où les spectres à analyser sont les spectres de l'exemple 1 (copie de fichiers interne plutôt qu'upload des spectres utilisateur)
        extract_files_from_list([common.SPECTRA_EXAMPLE_1], abs_spectra_dir)
    elif 'spectra_files' in form:
        # spectres utilisateurs 
        fileitems = form['spectra_files']
        if not isinstance(fileitems, list):
            fileitems = [fileitems]
        if fileitems[0].filename == "":
            error_messages.append("At least one spectrum is required.")
        else:
            uploaded, not_uploaded = extract_files_from_fileitems(fileitems, abs_spectra_dir, extensions_list=[".csv", ".mgd", ".mzml"], unzip=True)
            req['files_uploaded'].extend(uploaded)
            for fn in uploaded:
                log.write(f"file {fn}\n")
            for fn in not_uploaded:
                if fn in uploaded :
                    error_messages.append(f'The mass spectrum {fn} appears to be present several times in the data supplied.')
                else:
                    error_messages.append(f"{fn} : Spectra must be in CSV, MGD or mzML format or in a ZIP archive.")
    else:
        error_messages.append("At least one spectrum is required.")
    
    # Mass error parsing
    if "error_margin_selection" in form:
        error_margin_selection = form['error_margin_selection'].value
        error_margin = 0
        if error_margin_selection == "MALDI_TOF":
            # valeur par défaut
            error_margin = 50
        elif error_margin_selection == "MALDI_FTICR":
            # valeur par défaut
            error_margin = 5
        elif error_margin_selection == "ppm":
            if "error_margin_ppm" in form:
                error_margin = int(form['error_margin_ppm'].value)
                if error_margin < 1 or error_margin > 1000:
                    error_messages.append(f"The mass error in ppm must be within the range 1-1000. Input value: {error_margin}.")
            else:
                error_messages.append(f"With the custom value in ppm option, a mass error value is required.")
        elif error_margin_selection == "daltons":
            if "error_margin_daltons" in form:
                error_margin = float(form['error_margin_daltons'].value)
                if not (error_margin > 0 and error_margin < 1):
                    error_messages.append(f"The mass error in daltons must be within the range 0.002-0.998. Input value: {error_margin}.")
            else:
                error_messages.append(f"With the custom value in Daltons option, a mass error value is required.")
        else:
            error_messages.append(f"Internal error. An unknow value have been entered in the 'error_margin_selection' input: {error_margin_selection}")
        log.write(f"Error margin: {error_margin}\n")
        req['error_margin'] = error_margin
    else:
        error_messages.append("A mass error selection is necessary for the analysis.")
    
    # 'Choice of the markers and organisms' parsing (default data or upload custom data)
    if "reference_source" not in form :
        log.write(f"Reference source : default\n")  # log
        req['reference'] = "peptides"  # for the command
        
        # Taxonomic constraint (to write in the limit file or by selecting the reference data added in the result directory)
        req["limit_OX"] = []
        i = 1
        # pattern pour matcher avec la structure des sélection d'espèce/parent manuelle avec autocomplétion sur le formulaire
        pattern = re.compile("[a-zA-Z]+( [a-zA-Z]+)* (\([a-zA-Z]+( [a-zA-Z]+)*\) )?- \[[0-9]+; [a-zA-Z]+\]")
        while f"taxonomic_node_{i}" in form:
            if form[f'taxonomic_node_{i}'].value != "":
                taxonomic_node = form[f'taxonomic_node_{i}'].value
                log.write("Organism custom selection\n")  # log
                
                if pattern.search(taxonomic_node):  # The taxonomic node is entered with the right syntax
                    taxid = taxonomic_node.split(";")[0].split("[")[1]
                    req['limit_OX'].append(taxid)  # for the command
                    log.write(f"{taxid}\n")  # log
                else:
                    error_messages.append(f"The following taxonomic constraint does not respect the syntax criteria proposed by autocompletion: {taxonomic_node}")
            i += 1

        if len(req['limit_OX']) > 0:
            # Si une contrainte taxonomique a été appliquée manuellement, on prend en compte tous les jeux de données.
            tax_groups = ["mammals"]  # "birds", "fishes", "reptiles" seront dans cette liste aussi quand ils seront ajoutés aux données par défaut
                
        elif "taxonomic_group_selection" in form:
            # sinon, on prend en compte la sélection (case à (dé)cocher ) des grands groupes taxonomiques: mammals, birds, fish, reptiles
            items = form["taxonomic_group_selection"]
            if not isinstance(items, list):
                items = [items]
            tax_groups = [item.value for item in items if item.value in peptides_path_dict]
            log.write(f"taxonomic group {'/'.join(tax_groups)}\n")  # log
            if len(tax_groups) == 0:
                tax_groups = ["mammals"]  # To prevent the script from crashing
                error_messages.append("In the 'Organisms' field, you need to choose at least one taxonomic group (Mammals, Birds, Fishes and/or Reptiles) or select a set of organisms.")

        else:
            tax_groups = ["mammals"]  # To prevent the script from crashing
            error_messages.append("In the 'Organisms' field, you need to choose at least one taxonomic group (Mammals, Birds, Fishes and/or Reptiles) or select a set of organisms.")
        
        # Récupération des jeux de données internes pour l'analyse
        req["peptides_files"] = []  # for the command
        f_list = [peptides_path_dict[group] for group in tax_groups]
        extract_files_from_list(f_list, resdir)
        for f in f_list:
            fn = os.path.basename(f)
            req["peptides_files"].append(resdir + '/' + fn)  # for the command
            log.write(f"file {fn}\n")  # log
            

        # Taxonomy selection : fichier interne
        f = common.TAXONOMY_REDUCED_FILE
        fn = os.path.basename(f)
        extract_files_from_list([f], resdir)
        req["taxo_file"] = resdir + '/' + fn  # for the command
        log.write(f"file {fn}\n")  # log
        req["taxo_source"] = "default_reduced"
        
        # Deamidation parsing (for limitfile)
        if "ptm_deamidation" not in form:
            req["limit_PTM"] = "O"  # si 'include deamidation' non sélectionné, la seule PTM autorisée est l'hydroxylation (O)
            log.write("limit PTM: O\n")  # log

    else:
        # Données utilisateurs : table de peptides OU séquences de collagène
        log.write(f"Reference source : user\n")  # log

        # Reference data parsing (peptides OR sequences)
        if "peptides_files" in form and "sequences_files" in form:
            peptides_fileitems = form['peptides_files']
            sequences_fileitems = form['sequences_files']
            if not isinstance(peptides_fileitems, list):
                peptides_fileitems = [peptides_fileitems]
            if not isinstance(sequences_fileitems, list):
                sequences_fileitems = [sequences_fileitems]

            if (peptides_fileitems[0].filename == "" and sequences_fileitems[0].filename == "") or (peptides_fileitems[0].filename != "" and sequences_fileitems[0].filename != ""):
                error_messages.append("You need to provide either peptide table(s) or protein sequence(s).")
            elif peptides_fileitems[0].filename != "":  # Peptide table(s) provided
                # Table de peptides
                req['peptides_files'] = []  # for the command
                uploaded, not_uploaded = extract_files_from_fileitems(peptides_fileitems, resdir, extensions_list=[".tsv"], unzip=True)
                req['files_uploaded'].extend(uploaded)  # for the render_result
                req["reference"] = "peptides"  # for the command
                log.write(f"Reference type: {req['reference']}\n")  # log
                for fn in uploaded:
                    log.write(f"file {fn}\n")  # log
                    req['peptides_files'].append(resdir+'/'+fn)  # for the command
                for fn in not_uploaded:
                    if fn in uploaded :
                        error_messages.append(f'The mass spectrum {fn} appears to be present several times in the data supplied.')
                    else:
                        error_messages.append(f"{fn} : Peptide tables must be in TSV format or in a ZIP archive.")
            else:  # Sequence(s) provided
                # Séquences
                rel_sequences_dir = "sequences"
                abs_sequences_dir = os.path.join(resdir,rel_sequences_dir)  # destination directory
                req['sequences_dir'] = abs_sequences_dir  # for the command
                uploaded, not_uploaded = extract_files_from_fileitems(sequences_fileitems, abs_sequences_dir, extensions_list=[".fasta", ".fna", ".fa"], unzip=True)
                req['files_uploaded'].extend(uploaded)  # for the render_result 
                req["reference"] = "sequences"  # for the command
                log.write(f"Reference type: {req['reference']}\n")  # log
                for fn in uploaded:
                    log.write(f"file {fn}\n")  # log
                for fn in not_uploaded:
                    if fn in uploaded :
                        error_messages.append(f'The mass spectrum {fn} appears to be present several times in the data supplied.')
                    else:
                        error_messages.append(f"{fn} : Protein sequences must be in FASTA format or in a ZIP archive.")
        else:
            error_messages.append("Internal error. Issue in the formular structure ('peptides_files' or 'sequences_files' input).")
        
        # Taxonomy parsing
        if "taxonomy_selection" in form:
            taxonomy_selection = form["taxonomy_selection"].value
            if taxonomy_selection == "default":
                # sur les données (table/sequences) utilisateurs, on utilise la taxonomy_all.tsv qui est plus générale.
                f = common.TAXONOMY_ALL_FILE
                fn = os.path.basename(f)
                extract_files_from_list([f], resdir)
                req["taxo_file"] = resdir + '/' + fn  # for the command
                req["taxo_source"] = "default_all"
                log.write(f"file {fn}\n")  # log
            elif taxonomy_selection == "no":
                # pas d'information taxonomique -> pas de fichier taxo.tsv
                log.write("No taxonomy\n")
            elif taxonomy_selection == "custom":
                # upload de la taxonomie utilisateur si possible, sinon erreur
                if 'taxo_file' in form and not isinstance(form['taxo_file'], list) and form['taxo_file'].filename != "":
                    f = form['taxo_file'].filename
                    fn = os.path.basename(f)
                    uploaded, not_uploaded = extract_files_from_fileitems([form['taxo_file']], resdir, extensions_list=[".tsv"], unzip=False)
                    req["taxo_file"] = resdir + '/' + fn  # for the command
                    req["taxo_source"] = "user"
                    req['files_uploaded'].extend(uploaded)  # for the render_result
                    for fn in uploaded:
                        log.write(f"file {fn}\n")
                    for fn in not_uploaded:
                        error_messages.append(f"{fn} : The taxonomy file must be in TSV format.")
                else:
                    error_messages.append("If you choose to upload your own taxonomy, a taxonomy file is required.")
            else:
                error_messages.append(f"Internal error. An unexpected value has been submitted in the 'taxonomy_selection' input : {taxonomy_selection}")
        else:
            error_messages.append("A choice is required in the 'Taxonomy' section.")

    # option -n nombre et/ou -a (solution suboptimales)
    if 'nearoptimal_selection' in form:
        nearoptimal_selection = form["nearoptimal_selection"].value
        if nearoptimal_selection == "optimal":
            # rien à faire, solutions optimales par défaut
            log.write("Only optimal results\n")
        elif nearoptimal_selection == "nearoptimal":
            # seuil de suboptimalité à renseigner
            log.write("Near-optimal results\n")
            if form["nearoptimal_margin"].value != "":
                req['nearoptimal_margin'] = form["nearoptimal_margin"].value
                log.write(f"suboptimality percentage: {req['nearoptimal_margin']}%")
            else:
                error_messages.append("If you select near-optimal solutions, a suboptimality percentage is required.")
        elif nearoptimal_selection == "nearoptimal_all":
            # seuil de suboptimalité à renseigner + option all
            log.write("Near-optimal results (all)\n")
            req['nearoptimal_all'] = True
            if form["nearoptimal_all_margin"].value != "":
                req['nearoptimal_margin'] = form["nearoptimal_all_margin"].value
                log.write(f"suboptimality percentage: {req['nearoptimal_margin']}%")
            else:
                error_messages.append("If you select near-optimal solutions, a suboptimality percentage is required.")
        else:
            error_messages.append(f"Internal error. An unknow value has been submitted in the 'nearoptimal_selection' input : {nearoptimal_selection}")
    else:
        error_messages.append("A choice is required in the 'Results' part.")

    # écriture du limit_file
    if ("limit_OX" in req and len(req["limit_OX"]) > 0) or "limit_PTM" in req:
        req['limit_file'] = resdir+"/limitfile.txt"
        with open(resdir+"/limitfile.txt", "w") as fOut:
            if len(req["limit_OX"]) > 0:
                fOut.write(f"OX={','.join(req['limit_OX'])} ")
            if "limit_PTM" in req:
                fOut.write(f"PTM={req['limit_PTM']} ")
            fOut.write("\n")
    
    log.write("Recap downloads :\n")
    for fn in req['files_uploaded']:
        log.write(f"dwl {fn}\n")

    req["error_messages"] = error_messages
    log.write("Error messages :\n")
    for e in error_messages:
        log.write(f"error: {e}\n")
    log.close()
    return req
    
   


#Lance le programme en fonction des données contenant dans req
#Fonction à modifier pour l'adapter à votre programme
def launch_software(run_id, req):
    # Build the command line for PAMPA
    spectra_arg = req['spectra_dir']
    output_arg = "out_" + run_id + ".tsv"
    error_margin = req["error_margin"]
    files_uploaded = req['files_uploaded']
    command = "python pampa_classify.py -s " + spectra_arg + " -e " + str(error_margin) + " -o " + common.RESULT_DIR + run_id + "/"+output_arg
    if 'taxo_file' in req:
        command += " -t " + req['taxo_file']
    if req["reference"] == "peptides":
        command += " -p"
        for file in req['peptides_files']:
            command += " " + file
    else :  # 'sequences'
        command += " -d " + req['sequences_dir']
    if "limit_file" in req:
        command += ' -l ' + req['limit_file']
    if "nearoptimal_margin" in req:
        command += ' -n ' + req["nearoptimal_margin"]
        if "nearoptimal_all" in req:
            command += ' -a '
    command += " --web"  # Web version with log file and no console output
    open("acommand.txt", "w").write(f"{command}")  # log
    

    # Launch the analysis
    cwd = format(os.getcwd())
    os.chdir(format(os.getcwd()) + "/pampa/")
    os.system(command)
    os.chdir(cwd)

    # Écrit un fichier de résultats d'erreurs si PAMPA a planté, sinon affiche les résultats
    if open(common.RESULT_DIR + run_id + "/error.log", "r").read() != "":
        # Manage PAMPA errors (error.log)
        json_file = common.RESULT_DIR + run_id + "/render_error_pampa.json"
        os.system(f"python render_error_pampa.py {run_id}")

    else:
        # Params in json for render_result.py -> le json est écrit dans le dossier result/{run_id}
        json_file = common.RESULT_DIR + run_id + "/render_result_params.json"
        render_result_dict = {"run_id": run_id}
        if "job_name" in req:
            render_result_dict["job_name"] = req['job_name']  # nom du job
        if 'taxo_file' in req:
            render_result_dict["taxo_file"] = req["taxo_file"]  # taxonomie utilisée
            render_result_dict["taxo_source"] = req["taxo_source"]  # origine de la taxonomie: user / default_reduced / default_all
        else:
            render_result_dict["taxo_source"] = "none" # taxonomie non utilisée
        if req["reference"] == "peptides":
            render_result_dict["data_tables"] = req['peptides_files']  # tables de peptides utilisées
        else :  # 'sequences'
            render_result_dict["data_dir"] = req['sequences_dir']  # séquences fasta utilisées
        if "limit_OX" in req and len(req["limit_OX"]) > 0 and "limit_file" in req:
            render_result_dict["limit"] = True   # fichier de limites  i.e. limitfile  utilisé?
        else:
            render_result_dict["limit"] = False
        json.dump(render_result_dict, open(json_file, "w"))
        os.system(f"python render_result.py {json_file}")


#Vérifie si il n'y a pas d'erreur au retour des données soumises par le formulaire
def process_request(form):
    run_id = form['run_id'].value
    error=0
    error_page=[]
    
    req = extract_request(form)
    
    if len(req['error_messages'])>0:
        error_page=req['error_messages']
        error=1
        json_file = common.RESULT_DIR + run_id + "/render_error_params.json"
        render_error_dict = {"result_path": common.RESULT_DIR, "run_id": run_id, "error_list": req['error_messages']}
        json.dump(render_error_dict, open(json_file, "w"))
        os.system(f"python render_error.py {json_file}")
    else :
        launch_software(run_id, req)

    return (error, error_page)



def main():

    fs = cgi.FieldStorage()
    
    error, error_page=process_request(fs)

    sys.stdout.write("Content-Type: application/json")

    sys.stdout.write("\n")
    sys.stdout.write("\n")

    result={}
    if error==0 or error==1:
        result['success'] = True
        result['run_id'] = fs['run_id'].value
        result['path_result'] = common.RESULT_DIR+fs['run_id'].value+"/results.php"
    else:
        result['success'] = False

    sys.stdout.write(json.dumps(result, indent=1))
    sys.stdout.write("\n")


main()

