#!/usr/bin/python
# -*- coding: utf-8 -*-
import cgitb; cgitb.enable()
import os
import sys
import common
import cgi

def href(link, name):
        result = "<a href='"+link+"' class='normal_link' target='_blank'>"+name+"</a>"
        return result

def li(item):
	return "<li>"+item+"</li>"

def ul(items):
	return "<ul>"+items+"</ul>"

def magnify_style():
        style = '''

<style>

.thumbnail {

        -webkit-transition: all 1s ease; /* Safari and Chrome */
        -moz-transition: all 1s ease; /* Firefox */
        -ms-transition: all 1s ease; /* IE 9 */
        -o-transition: all 1s ease; /* Opera */
        transition: all 1s ease;

}
.big {

        -webkit-transform:scale(3.5); /* Safari and Chrome */
        -moz-transform:scale(1.25); /* Firefox */
        -ms-transform:scale(1.25); /* IE 9 */
        -o-transform:scale(1.25); /* Opera */
        transform:scale(3.5);
}

/* zoom */
* {margin: 0; padding: 0;}
.magnify {width: 350px; margin: 50px auto; position: relative;}

/*Lets create the magnifying glass*/
.large {
        width: 200px; height: 200px;
        position: absolute;
        border-radius: 100%;

        /*Multiple box shadows to achieve the glass effect*/
        box-shadow: 0 0 0 7px rgba(255, 255, 255, 0.85), 
        0 0 7px 7px rgba(0, 0, 0, 0.25), 
        inset 0 0 40px 2px rgba(0, 0, 0, 0.25);

        /*Lets load up the large image first*/  
        /*        d      finir dynamiquement en javascript */
        /*background: url('') no-repeat;*/

        /*hide the glass by default*/
        display: none;

        z-index: 1;
}

/*To solve overlap bug at the edges during magnification*/
.small { display: block; }
</style>
'''


        return style;

