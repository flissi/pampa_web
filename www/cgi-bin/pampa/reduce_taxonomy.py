#!/usr/bin/python
# -*- coding: utf-8 -*-

# Pourrait être adapté et utilisé pour automatiser la maintenance/mise à jour du fichier taxonomy_reduced.tsv au regard des données de table de peptides en lançant ce script dans le Dockerfile ?

"""# Modules import
import os
import common


# Functions definition
def command_builder(script, taxonomy, output="taxonomy_reduced.tsv", python="python3", peptides_tables=None, sequence_dir=None):
    if os.path.splitext(output) == "":
        output += ".tsv"
    command = f"{python} {script} -t {taxonomy} -o {output}"
    if peptides_tables and len(peptides_tables) > 0:
        command += f" -p {' '.join(peptides_tables)}"
    if sequence_dir:
        command += f" -d {sequence_dir}"
    return command


# main
if __name__ == "__main__":
    script = common.CGI_PATH+"/reduce_taxo/main_taxonomy_filtering.py"
    taxonomy = common.TAXONOMY_ALL_FILE
    peptides_tables = [common.PEPTIDES_MAMMALS_FILE, common.PEPTIDES_BIRDS_FILE, common.PEPTIDES_FISHES_FILE, common.PEPTIDES_REPTILES_FILE]

    command = command_builder(script, taxonomy, output=common.TAXONOMY_REDUCED_FILE, peptides_tables=peptides_tables, python="python")

    os.system(command)"""