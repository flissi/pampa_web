#!/usr/bin/env python3
import cgitb; cgitb.enable()
import os.path
import shutil
import sys
import logging
from os import listdir
import psycopg2
from config import load_config
import json


def gen_sub_json(taxo_path, id, name):
    config = load_config()
    sql = "SELECT * FROM taxonomy"
    try:
        with  psycopg2.connect(**config) as conn:
            with  conn.cursor() as cur:
                create_json(cur, taxo_path, id, name)
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)

def create_json(cur, taxo_path, id, name):
    json_array = [ ]
    json_root = { "id" : id, "text" : name, "children" : []}
    children = [] # 1 seul element a racine de l'arbre!
    state = { "opened" : "false", "disabled" : "true" }
    json_root["state"] = state

    # on construit children en ajoutant l'element racine -> taxonId = -1 (root)
    add_childs(cur, json_root, id)

    json_array.append(json_root)
    with open(taxo_path, 'w') as file:
        json.dump(json_array, file, indent=4)

def add_childs(cur, json, id):
    sql = "SELECT * from taxonomy WHERE parent = " + str(id)
    childrens = []
    try:
        cur.execute(sql)
        rows = cur.fetchall()
        idx = 0
        for row in rows:
            new_id = row[0]
            text = row[2]
            json_object = {"id" : new_id, "text" : text, "children" : []}
            state = { "opened" : "false", "disabled" : "true" }
            json_object["state"] = state
            add_childs(cur, json_object, new_id)
            childrens.append(json_object)
        json["children"] = childrens

    except (Exception, psycopg2.DatabaseError) as error:
        print(error)

