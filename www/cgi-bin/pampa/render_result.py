import cgitb; cgitb.enable()
import json
import os, sys
from html_utils import *
import common
import zipfile
import operator
from render_sub_assignment import *

"""
NOTES

- en ajoutant de nouvelles tables de peptides par défaut autres que mammals dans nos données, il faut adapter dans la partie de génération de l'arbre taxonomique section 'reduction by taxonomic groups' la vérification que toutes les tables (groupes taxonomiques) ont été inclues.

"""



def href_dl(link, name):
    """
    Construit une balise <a> ouvrant la page sur un nouvel onglet.
    """
    result = f'<a href="{link}" download="{name}" target="_blank">{name}</a>'
    return result

def command_builder_taxoreducer(taxonomy, output, reroot=False, peptides_tables=None, sequence_dir=None, limit_file=None):
    """
    Construit la ligne de commande adaptée à l'utilisation du script 'main_taxonomy_reduced'. Ce script produit une taxonomie réduite sur la base de l'ensemble des espèces sur lesquelles des données sont renseignées (sous la forme de tables de peptides ou d'un dossier de fichiers fasta de séquences de collagène) ou d'un limit_file (au format employé par PAMPA).
    L'option reroot permet de faire descendre la racine de l'arbre au plus petit ancêtre commun à toutes les feuilles de l'arbre.
    """
    if os.path.splitext(output) == "":
        output += ".tsv"
    command = f"/usr/bin/python {common.PAMPA_DIR}main_taxonomy_filtering.py -t {taxonomy} -o {output}"
    if peptides_tables and len(peptides_tables) > 0:
        command += f" -p {' '.join(peptides_tables)}"
    if sequence_dir:
        command += f" -d {sequence_dir}"
    if limit_file:
        command += f" -l {limit_file}"
    if reroot:
        command += " -r"
    return command

def zip_results(file_names, run_id, job_name=None):
    """
    Produit un fichier ZIP contenant tous les fichiers de résultat : 
    - out.tsv
    - detail_out.tsv
    - report_out.txt
    Ce fichier ZIP pourra être téléchargé par l'utilisateur.
    """
    with zipfile.ZipFile(f"{common.RESULT_DIR}{run_id}/data_result_{run_id}.zip", "a", compression=zipfile.ZIP_DEFLATED) as dwl_zip:
        for fn in file_names:
            abs_path = common.RESULT_DIR + run_id + '/' + fn
            if os.path.exists(abs_path):
                if job_name is not None:
                    parts = os.path.splitext(fn)
                    dwl_name = parts[0] + "_" + job_name + parts[1]
                else: 
                    dwl_name = fn
                dwl_zip.write(abs_path, arcname=dwl_name)

def results_output(run_id, job_name=None):
    """
    Produit la liste des fichiers téléchargeables par l'utilisateur.
    """
 
    output_files_title = {
        "out_"+run_id+".tsv": "Assignments: ",
        "detail_out_"+run_id+".tsv": "More details: ",
        "report_out_"+run_id+".txt": "Report on the run: ",
        "table_out_"+run_id+".tsv": "Peptides table built: ",
        "data_result_"+run_id+".zip": "Downloads all results and data: "
    }
    head_out = "All results are available in the following files."
    html = head_out
    
    html += "<ul>"
    listdir = os.listdir(path=common.RESULT_DIR+run_id)
    for fn in output_files_title:
        if fn in listdir:
            if job_name is not None:
                parts = os.path.splitext(fn)
                dwl_name = parts[0] + "_" + job_name + parts[1]
            else: 
                dwl_name = fn
            html += li(output_files_title[fn] + href_dl(fn, dwl_name))
    html += "</ul>"

    return  html

def assignments_output(run_id, taxo_used, taxo_info, job_name=""):
    """
    Produit un tableau HTML recensant les informations essentielles. Chaque ligne est cliquable pour accéder aux détails.
    Deux versions de ce tableau peuvent être générées en fonction de l'usage d'une taxonomie ou non pas PAMPA lors de l'analyse.
    """

    html = ''
    html += '<table id="result_pampa" class="result" style="font-size:11px; width:100%;">'

    content = json.load(open(common.RESULT_DIR + run_id + "/" + "out_" + run_id + ".json", "r"))
    content.sort(key=lambda x: operator.itemgetter("spectrum_name", "pvalue")(x))

    # Table head
    html += '<tr>'
    html += '<th>Spectrum</th>'
    if taxo_used:
        html += '<th colspan="2">Assignment</th>'
        html += '<th colspan="2">Uncertainty</th>'
    else:
        html += '<th>Species</th>'
    html += '<th>#peaks</th>'
    html += '<th>P-value</th>'
    html += '<th></th>'
    html += '</tr>'

    spectrum_name = ""
    row_class = ("impair", "pair")
    i = 1
    for assignment in content:
        if taxo_used:
            row = [assignment['spectrum_name'], 
                assignment['lca_name'], assignment['lca_rank'], 
                assignment['hca_name'], assignment['hca_rank'], 
                assignment['score'], format(assignment['pvalue'], ".2E"), 
                '<a><B>View</B></a>']
        else:
            row = [assignment['spectrum_name'], 
                ", ".join([taxon["name"] for taxon in assignment["taxa"]]), 
                assignment['score'], 
                format(assignment['pvalue'], ".2E"), 
                '<a><B>View</B></a>']
        if spectrum_name != assignment['spectrum_name']:
            i += 1
            spectrum_name = assignment['spectrum_name']
        else:
            row[0] = "-"
            row[-1] = "-"
        td_row = "".join(f'<td class="link_img" data-spectrumname="{spectrum_name}" data-jobname="{job_name}" data-taxo="{taxo_info}">{e}</td>' for e in row)
        html += f'<tr class="{row_class[i%2]}">{td_row}</tr>'

    html += '</table>'
        
    return html

def write_main_page(run_id, taxo_used, taxo_info, no_assignments, job_name=None):
    """Write a HTML page. The result page shown first when the PAMPA analysis is done."""
    html = ""

    # Insert HTML header and page head
    html += (open(f"{common.HTML_PATH}/header.php", "r").read())
    html += '<div class="frametitle"><h1 id="title">Pampa</h1></div><div id="center_sup"><div class="theme-border" style="display:none"></div><div id="link_home" style="display:inline-block"><a href="/" class="text_onglet"><img src="/Style/icon/home_w.png" alt="home_general"/></a></div><div class="tabs" id="menu_central" style="display:inline-block">'
    html += open(f"{common.HTML_PATH}/menu_central.txt", 'r').read()
    html += '</div></div><div id="main"><div id="center">'

    html += (f'<h2>Results for job {run_id}{f" ({job_name})" if job_name else ""}</h2>')

    # Show the warnings
    with open(common.RESULT_DIR+run_id+"/warning.log") as fileOut:
        content = fileOut.read().strip()
        if content != "":
            html += ('<div id="warnings_hidden" style="display: block;">')
            html += ('<p>Warning(s) were raised during the execution. <a id="show_warnings">View report</a></p>')
            html += ('</div>')
            html += ('<div id="warnings_shown" style="display: none;">')
            html += ('<p>Warning(s) were raised during the execution. <a id="hide_warnings">Hide</a></p>')
            html += ('<table class="vide">')
            for line in content.split('\n'):
                if line != "":
                    html += (f'<tr><td>{line}</tr></td>')
            html += ('</table>')
            html += ('</div>')
            html += ('<br>')


    # Show the assignments in a table
    html += ('<br>')
    html += (assignments_output(run_id, taxo_used, taxo_info, job_name=job_name))
    html += ('<br>')

    if no_assignments:
        html += '<h4><font color="red">No assignments found</font></h4>'

    # Display assignment thumbnails
    #html += ("<iframe src='thumbnails.php' width='100%' height='360'></iframe>")
    #html += ('<br/>')


    # Download the result files
    html += ("<h3>Download</h3>")
    html += (results_output(run_id, job_name=job_name))
    html += ('<br>')

    # Come back later
    html += '<h3>Retrieve results with an ID</h3>'
    html += '<p>Your result remains available for at least one month.</p>'
    html += f'<p>To access it, you can either save the url or <a id="copy" data-copy="{run_id}">copy the ID</a> {run_id}.</p>'
    html += ('<br>')

    # Insert page footer
    html += '</div></div>'
    html += open(f"{common.HTML_PATH}/footer.php", "r").read()
    html +='</body></html>'

    open(common.RESULT_DIR + run_id + "/results.php", "w").write(html)


def gen_trees(run_id, taxo_custom=False):
    json_content = json.load(open(common.RESULT_DIR + run_id + "/out_" + run_id +".json"))
    if taxo_custom:
        url_json = f"/pampa/result/{run_id}/taxonomy_{run_id}.json"
    else:
        url_json = "/pampa/data_pampa/taxonomy_reduced.json"
    for assignment in json_content:
        assign = assignment["lca_name"]
        gen_sub_tree(common.RESULT_DIR, run_id, assign.replace(" ", "_"), assignment, "#0000FF", url_json=url_json)


def write_no_results_page(run_id, job_name=None):
    """Write a HTML page. The result page shown first when the PAMPA analysis is done."""
    html = ""

    # Insert HTML header and page head
    html += (open(f"{common.HTML_PATH}/header.php", "r").read())
    html += '<div class="frametitle"><h1 id="title">Pampa</h1></div><div id="center_sup"><div class="theme-border" style="display:none"></div><div id="link_home" style="display:inline-block"><a href="/" class="text_onglet"><img src="/Style/icon/home_w.png" alt="home_general"/></a></div><div class="tabs" id="menu_central" style="display:inline-block">'
    html += open(f"{common.HTML_PATH}/menu_central.txt", 'r').read()
    html += '</div></div><div id="main"><div id="center">'

    html += (f'<h2>Results for job {run_id}{f" ({job_name})" if job_name else ""}</h2>')

    html += '<h4><font color="red">No results</font></h4>'
    open(common.RESULT_DIR + run_id + "/results.php", "w").write(html)


# Main program
def main():
    # Ce script est lancé avec un paramètre : le nom d'un fichier json
    # ce fichier contient diverses informations qui vont guider la construction de la page de résultats.
    json_params_file = sys.argv[1]
    params = json.load(open(json_params_file, "r"))
    run_id = params["run_id"]

    if "job_name" in params:
        job_name = params['job_name']
    else:
        job_name = None
    taxo_source = params["taxo_source"]
    if "limit" in params:
        limit = params['limit']
    else:
        limit = False
    if "data_tables" in params:
        data_tables = params['data_tables']
        data_dir = None
    else:  # data_dir
        data_tables = None
        data_dir = params['data_dir']

    # production du fichier ZIP contenant les résultats
    zip_results(["out_"+run_id+".tsv", "detail_out_"+run_id+".tsv", "report_out_"+run_id+".txt", "table_out_"+run_id+".tsv"], run_id, job_name=job_name)

    # test de l'usage d'une taxonomie par PAMPA
    if taxo_source == "none":
        taxo_used = False
    else:
        taxo_file = params["taxo_file"]
        assignments = json.load(open(common.RESULT_DIR+"/"+ run_id + "/out_" + run_id +".json"))
        no_assignments = False
        if len(assignments) == 0:
            no_assignments = True
            #write_no_results_page(run_id, job_name)
            taxo_used = False
        else:
            taxo_used = (assignments[0]["lca"] != None)
        del assignments

    # arbre global
    #gen_tree(common.RESULT_DIR, run_id)

    # Sous-arbres taxonomiques
    # usage d'un fichier TXT de log présent le cgi-bin pour suivre l'évolution d'une analyse et début si besoin
    log_tree = open("log.txt", "a")
    tree_created = taxo_used
    if no_assignments != True:
        log_tree.write("PAMPA is using a taxonomy. Preparing the tree(s)...\n")
        try:
            if taxo_source == "default_reduced":
                #Usage de la taxonomie réduite sur les données par défaut de PAMPA (taxonomy_reduced.tsv)
                log_tree.write("using the taxonomy reduced\n")

                if f"{common.RESULT_DIR}{run_id}/{os.path.basename(common.PEPTIDES_MAMMALS_FILE)}" in data_tables: # if taxo selection by group 
                    # réduction par groupe taxonomique (si par exemple sur les jeux de données poissons et mammifères, seuls les poissons sont utilisés pour l'analyse.)
                    log_tree.write("reduction by taxonomic groups\n")
                    taxo_reduced_file = f"{common.RESULT_DIR}{run_id}/taxonomy_{run_id}.tsv"
                    command_reduce = command_builder_taxoreducer(taxo_file, taxo_reduced_file, peptides_tables=data_tables, reroot=True)
                    log_tree.write(f"reduce taxo: {command_reduce}\n")
                    os.system(command_reduce)  # reduces the taxonomy from the taxonomy_reduced.tsv
                    taxo_custom = True

                elif limit: # if limits taxo from the limit_file - reduction by limit file
                    # réduction sur la base d'un fichier de limite (incompatible avec la sélection d'un groupe taxonomique ci-dessus)
                    log_tree.write("reduction by limit file\n")
                    limit_file = f"{common.RESULT_DIR}{run_id}/limitfile.txt"
                    taxo_reduced_file = f"{common.RESULT_DIR}{run_id}/taxonomy_{run_id}.tsv"
                    command_reduce = command_builder_taxoreducer(taxo_file, taxo_reduced_file, limit_file=limit_file, peptides_tables=data_tables, reroot=True)
                    log_tree.write(f"reduce taxo: {command_reduce}\n")
                    os.system(command_reduce)  # reduces the taxonomy from the taxonomy_reduced.tsv
                    taxo_custom = True

                else : # le fichier taxo_reduced est utilisable tel quel - no reduction
                    log_tree.write("no reduction\n")
                    taxo_custom = False
            else: # data from the user (peptide table or sequences)
                #reduction de la taxonomie la plus générale (taxonomy_all.tsv) sur les données utilisateur
                taxo_reduced_file = f"{common.RESULT_DIR}{run_id}/taxonomy_{run_id}.tsv"
                if data_tables:
                    # reduction sur tables de peptides
                    log_tree.write("reduction on custom peptide tables\n")
                    command_reduce = command_builder_taxoreducer(taxo_file, taxo_reduced_file, peptides_tables=data_tables, reroot=True)
                    log_tree.write(f"reduce taxo: {command_reduce}\n")
                else: # data_dir is not None
                    # reduction sur sequences de collagene (dossier de fichiers FASTA)
                    log_tree.write("reduction on custom sequences\n")
                    command_reduce = command_builder_taxoreducer(taxo_file, taxo_reduced_file, sequence_dir=data_dir, reroot=True)
                    log_tree.write(f"reduce taxo: {command_reduce}\n")
                os.system(command_reduce)  # reduces the taxonomy from the taxonomy_reduced.tsv
                taxo_custom = True

            # Si la taxonomie utilisée pour les arbres
            if taxo_custom:
                log_tree.write("reduction passed\n")
                command_bdd = f"/usr/bin/python {common.POSTGRESQL_DIR}fill_taxo.py {taxo_reduced_file} {run_id}"
                log_tree.write(f"bdd: {command_bdd}\n")
                os.system(command_bdd)  # creates a BDD from the taxonomy reduced previously
                log_tree.write("bdd passed\n")
                command_json = f"/usr/bin/python {common.POSTGRESQL_DIR}json_taxo.py {common.RESULT_DIR+run_id} {run_id}"
                log_tree.write(f"json creation: {command_json}\n")
                os.system(command_json)  # creates a json from the BDD
                log_tree.write("json passed\n")

            gen_trees(run_id, taxo_custom=taxo_custom)
            

        except:
            # pas d'arbre javascript créé, pas d'affichage sur la page de détails
            tree_created = False
            log_tree.write(f"Tree generation failed.\n")


    else:
        # pas d'arbre javascript créé, pas d'affichage sur la page de détails
        tree_created = False
        log_tree.write(f"\nNo tree was generated because PAMPA did not use any taxonomy for its analysis.\n")
    log_tree.close()

    #gen_thumbs(common.RESULT_DIR, run_id)

    # taxo_info donne l'information de la taxonomie utilisée pour les arbres : 'default' pour taxonomy_reduced, 'custom' si une taxonomie a été retravaillée et réduite sur les données d'analyse, 'no' si aucun arbre n'a été généré.
    # Cette information est écrite dans le tableau de résultats pour être transmise lors de la génération de la page de détails.
    if tree_created:
        if taxo_custom:
            taxo_info = "custom"
        else:
            taxo_info = "default"
    else:
        taxo_info = "no"
    write_main_page(run_id, taxo_used, taxo_info, no_assignments, job_name=job_name)


main()

