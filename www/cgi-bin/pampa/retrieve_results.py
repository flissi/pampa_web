#!/usr/bin/python
# -*- coding: utf-8 -*-

# Modules import
import cgitb; cgitb.enable()
import os
import cgi
import common
import sys
import json

from html_utils import *
from render_sub_assignment import *

# Functions definition
def extract_request(form):
    error = 0
    error_messages = []
    url_result = ""

    if "run_id" in form:
        run_id = form["run_id"].value
    else:
        error = 1
        error_messages.append("The script need the parameter 'run_id'.")
        run_id = ""

    if error == 0:
        if os.path.exists(f'{common.RESULT_DIR}{run_id}/results.php'):
            url_result = f'result/{run_id}/results.php'
        else:
            url_result = 'fail_retrieve.php'

    return error, error_messages, url_result, run_id
    



# Main
def main():
    """
    Ce script redirige l'utilisateur vers la page de résultats associée au run_id renseigné si cette page existe, sinon une page d'erreur est affichée.
    """
    
    form = cgi.FieldStorage()

    error, error_messages, url_result, run_id = extract_request(form)


    sys.stdout.write("Content-Type: application/json")
    sys.stdout.write("\n")
    sys.stdout.write("\n")
    result={}
    if error == 0:
        result['success'] = True
        result['url_result'] = url_result 
        result['comment'] = url_result
    else:
        result['success'] = False
        result['error_messages'] = error_messages
    sys.stdout.write(json.dumps(result, indent=1))
    sys.stdout.write("\n")

main()
