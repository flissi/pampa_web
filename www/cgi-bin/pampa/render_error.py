import cgitb; cgitb.enable()
import json
import os, sys
from html_utils import *
import common




def write_main_page(result_path, run_id, error_list):
    """Write a HTML page. This result page display errors made by the user when filling the formular."""
    main_page = open(result_path + run_id + "/results.php", "w")
    
    # Insert page header
    main_page.write(open(f"{common.HTML_PATH}/header.php", "r").read())
    main_page.write('<div class="frametitle"><h1 id="title">Pampa</h1></div><div id="center_sup"><div class="theme-border" style="display:none"></div><div id="link_home" style="display:inline-block"><a href="/" class="text_onglet"><img src="/Style/icon/home_w.png" alt="home_general"/></a></div><div class="tabs" id="menu_central" style="display:inline-block">')
    main_page.write(open(f"{common.HTML_PATH}/menu_central.txt", 'r').read())
    main_page.write('</div></div><div id="main"><div id="center">')

    # Show the error messages
    main_page.write("<div class='formulaire'>")
    main_page.write("<h2>The analysis has been interrupted because of the following issue(s)</h2>")
    main_page.write(ul("".join([li(error_msg) for error_msg in error_list])))
    main_page.write("</div>")
    main_page.write("<br/>")

    # Back to the formular
    main_page.write('<a href="/pampa/form.php" class="aLoad" >Back to PAMPA classify</a>')

    # Insert page footer
    main_page.write('</div></div>')
    main_page.write(open(f"{common.HTML_PATH}/footer.php", "r").read())
    main_page.write('</body></html>')

    main_page.close()


# Main program
def main():
    """
    Cette page affiche les erreurs rencontrées lors de la construction de la requête pour PAMPA dans 'pampa.py'. Ces erreurs ont été écrite dans le script 'pampa.py' et sont passées à ce script à travers un json.
    """

    # Parse the parameters (json) and set variables
    json_params_file = sys.argv[1]
    params = json.load(open(json_params_file, "r"))
    result_path = params["result_path"]
    run_id = params["run_id"]
    error_list = params["error_list"]

    write_main_page(result_path, run_id, error_list)

main()