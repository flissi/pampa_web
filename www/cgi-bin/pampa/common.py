#!/usr/bin/python
# -*- coding: utf-8 -*-

import os

#Remplacer exemple_web_server par le nom de votre logiciel
HTML_PATH = "/var/www/html/pampa"
CGI_PATH = "/var/www/cgi-bin/pampa"

TEMP_DIR = os.path.join(CGI_PATH, 'tmp/')
RESULT_DIR = os.path.join(HTML_PATH, "result/")
DATA_PAMPA_DIR = os.path.join(HTML_PATH, "data_pampa/")
POSTGRESQL_DIR = os.path.join(CGI_PATH, 'postgresql/')
PAMPA_DIR = os.path.join(CGI_PATH, 'pampa/')

# TODO /!\ Remplacer les tables de peptides des Birds Fishes et Reptiles par les vraies tables
PEPTIDES_MAMMALS_FILE = DATA_PAMPA_DIR + "table_mammals_with_deamidation.tsv"
PEPTIDES_BIRDS_FILE = DATA_PAMPA_DIR + ".tsv"
PEPTIDES_FISHES_FILE = DATA_PAMPA_DIR + ".tsv"
PEPTIDES_REPTILES_FILE = DATA_PAMPA_DIR + ".tsv"

TAXONOMY_ALL_FILE = DATA_PAMPA_DIR + "taxonomy_all.tsv"
TAXONOMY_REDUCED_FILE = DATA_PAMPA_DIR + "taxonomy_reduced.tsv"

SPECTRA_EXAMPLE_1 = DATA_PAMPA_DIR + "example_1.zip"


#Renvoie le pwd pour le dossier tmp/
def tmp_dir(run_id):    
    tmpdir = os.path.join(TEMP_DIR, run_id)
    return tmpdir


#Renvoie le pwd du dossier result/
def result_dir(run_id):    
    resdir = os.path.join(RESULT_DIR, run_id)
    return resdir


#Retourne le pwd du dossier où sont stockés les résultats de la requête
def result_dir_html(run_id):    
    resdir = os.path.join(RESULT_DIR, run_id)
    return resdir

#Retourne le pwd du fichier fasta contenant les séquences soumises
def fasta_path_result(run_id):
    result_dir = result_dir_html(run_id)
    fpath = os.path.join(result_dir, run_id + ".fasta")
    return fpath
