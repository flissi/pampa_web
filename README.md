
# What is pampa_web?

**pampa_web** is a web app that allow to execute the **pampa** software 

# URL

> [https://gitlab.cristal.univ-lille.fr/flissi/pampa_web/](https://gitlab.cristal.univ-lille.fr/flissi/pampa_web/)

# Clone repository

$ git clone https://gitlab.cristal.univ-lille.fr/flissi/pampa_web.git

# Adapt the Dockerfile to your host

Customize  SERVER_NAME value with the server and domain name of your host (default: localhost)

# Build the Docker image

$ docker build -t pampa:web . 


# Run the container

$ docker run -d -t -p 80:80 --name pampa pampa:web

You can then access the pampa web app at http://localhost/pampa or http://<server_name>/pampa/ from remote host
