#!/usr/bin/env python3
import os.path
import shutil
import sys
import logging
from os import listdir
import psycopg2
from config import load_config
import csv
import re




def create_table(table, verbose=False):
    config = load_config()
    sql = open(sys.path[0]+"/pampa.sql", "r").read().replace("\n", "").replace("taxonomy", table)
    try:
        with psycopg2.connect(**config) as conn:
            with conn.cursor() as cur:
                cur.execute(sql, (table))
                conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        if verbose:
            print(error) 

def load_file(path, run_id=None, verbose=False):
    # idx=0
    
    if run_id :
        runid_pattern = re.compile("^[A-Z][a-z]{2}_[0-9]{2}_[0-9]{4}_[0-9]{2}_[0-9]{2}_[0-9]{2}_[0-9]{1,5}$|^all$|^reduced$")  # Jul_05_2024_12_35_25_5220 or "reduced" or "all"
        assert runid_pattern.match(run_id)
        table = "taxonomy_" + run_id 
        create_table(table, verbose=verbose)
    else :
        table = "taxonomy"
    with open(path, 'r') as taxo_file:
        tsv_file = csv.reader(taxo_file, delimiter="\t")
        next(tsv_file)
        for line in tsv_file:
            # if idx == 10: break
            # idx+=1
            # taxoId = line[0]
            # commonName = line[1]
            # scientificName = line[2]
            # parentId = line[3]
            # rank = line[4]
            # print(line)
            insert_data(table, line, verbose=verbose)
        
        # add_root(table, path, verbose=verbose)
        # re_arrange(verbose=verbose)

def insert_data(table, raw, verbose=False):
    config = load_config()
    taxoId = raw[0]
    commonName = raw[1]
    scientificName = raw[2]
    rank = raw[4]
    #if rank=="no rank":
    #    parentId=-1
    #else:
    #    parentId=raw[3]
    parentId = raw[3]
    sql = "INSERT INTO "+table+" (taxonid, commonname, scientificname, parent, rank) VALUES(%s,%s,%s,%s,%s)"
    #print(sql)
    try:
        with psycopg2.connect(**config) as conn:
            with conn.cursor() as cur:
                cur.execute(sql, (taxoId, commonName, scientificName, parentId, rank))
                conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        if verbose:
            print(error)    

# def add_root(table, path, verbose=False):
#     config = load_config()
#     taxo = taxonomy.parse_taxonomy_simple_file(path)
#     root_id = str(taxo.root)
#     print(root_id)
#     update = "UPDATE "+table+" SET parent=-1 WHERE taxonid='%s'"
#     try:
#         with  psycopg2.connect(**config) as conn:
#             with  conn.cursor() as cur:
#                 cur.execute(update, (root_id))
#                 conn.commit()
#     except (Exception, psycopg2.DatabaseError) as error:
#         if verbose:
#             print(error)    

# def re_arrange(verbose=False):
#     config = load_config()
#     insert = "INSERT INTO taxonomy(taxonid, scientificname, parent) VALUES(%s,%s,%s)"  # update with table name as parameter
#     update = "UPDATE taxonomy set parent=0 WHERE scientificname like '%unclassified %'"
#     try:
#         with  psycopg2.connect(**config) as conn:
#             with  conn.cursor() as cur:
#                 cur.execute(insert, (-1, "Others", 0))
#                 cur.execute(update)
#                 conn.commit()
#     except (Exception, psycopg2.DatabaseError) as error:
#         if verbose:
#             print(error)   


if __name__ == '__main__':
    if len(sys.argv) == 2:
        load_file(sys.argv[1])  # taxonomy_path
    elif len(sys.argv) == 3:
        load_file(sys.argv[1], run_id=sys.argv[2])  # taxonomy_path, run_id
    elif len(sys.argv) == 4:
        load_file(sys.argv[1], run_id=sys.argv[2], verbose=True)  # 3e params to pass verbose to True