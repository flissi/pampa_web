#!/usr/bin/env python3
import os.path
import shutil
import sys
import logging
from os import listdir
import psycopg2
from config import load_config
import json
import re




def execute(taxo_path, run_id=None, verbose=False):
    # print("Generating taxonomy JSON file...")

    config = load_config()
    if run_id :
        runid_pattern = re.compile("^[A-Z][a-z]{2}_[0-9]{2}_[0-9]{4}_[0-9]{2}_[0-9]{2}_[0-9]{2}_[0-9]{1,5}$|^all$|^reduced$")  # Jul_05_2024_12_35_25_5220 or "reduced" or "all"
        assert runid_pattern.match(run_id)
        table = "taxonomy_" + run_id 
    else :
        table = "taxonomy"
    # sql = "SELECT * FROM %s"
    try:
        with psycopg2.connect(**config) as conn:
            with conn.cursor() as cur:
                create_json(cur, table, taxo_path, verbose=verbose)
                # cur.execute(sql, (table))
                # rows = cur.fetchall()
                # for row in rows:
                #    print("taxonId =", row[0])
    except (Exception, psycopg2.DatabaseError) as error:
        if verbose:
            print(error)
    # print("Done.")

def create_json(cur, table, taxo_path, verbose=False):
    json_array = [ ]
    json_root = { "id" : 0, "text" : "Taxonomy", "children" : []}
    children = [] # 1 seul element a racine de l'arbre!
    state = { "opened" : "false", "disabled" : "true" }
    json_root["state"] = state

    # on construit children en ajoutant l'element racine -> taxonId = -1 (root)
    add_childs(cur, table, json_root, -1, verbose=verbose)

    json_array.append(json_root)
    #print(json_array)
    json_name = "taxonomy_reduced.json" if table=="taxonomy" else table+".json"
    with open(os.path.join(taxo_path, json_name), 'w') as file:
        json.dump(json_array, file, indent=4)
    #f = open("taxonomy_mammals.json")
    #data = json.load(f)

def add_childs(cur, table, json, id, verbose=False):
    sql = "SELECT * from "+table+" WHERE parent = " + str(id)
    # print("Building childs for taxon", id)
    # print(sql)
    childrens = []
    try:
        cur.execute(sql)
        rows = cur.fetchall()
        # idx = 0
        # print("rowcount",cur.rowcount)
        for row in rows:
            # print(row)
            new_id = row[0]
            text = row[2]
            # print("taxonid = ", new_id, text)
            json_object = {"id" : new_id, "text" : text, "children" : []}
            state = { "opened" : "false", "disabled" : "true" }
            json_object["state"] = state
            add_childs(cur, table, json_object, new_id)
            childrens.append(json_object)
        json["children"] = childrens

    except (Exception, psycopg2.DatabaseError) as error:
        if verbose:
            print(error)
    # return json

if __name__ == '__main__':
    if len(sys.argv) == 2:
        execute(sys.argv[1])  # dir path for the taxo json created
    elif len(sys.argv) == 3:
        execute(sys.argv[1], sys.argv[2])  # path, run_id
    elif len(sys.argv) == 4:
        execute(sys.argv[1], sys.argv[2], verbose=True)  # 3e params to pass verbose to True
