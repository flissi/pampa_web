CREATE TABLE taxonomy (
        taxonId integer PRIMARY KEY NOT NULL,
        commonName character varying(200), 
        scientificName character varying(200),
        parent integer, 
        rank character varying(100)
)